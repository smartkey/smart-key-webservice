/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.exception;

/**
 *
 * @author Hossam ElDeen
 */
public class SmartKeyException extends RuntimeException{
    public SmartKeyException(String message) {
        super(message);
    }
}
