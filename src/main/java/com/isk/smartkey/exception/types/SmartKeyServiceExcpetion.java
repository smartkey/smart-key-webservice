/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.exception.types;

import com.isk.smartkey.exception.SmartKeyException;

/**
 *
 * @author Hossam ElDeen
 */
public class SmartKeyServiceExcpetion extends SmartKeyException {
    public SmartKeyServiceExcpetion(String message) {
        super(message);
    }
}
