/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.persistence.entities;

import java.util.Date;

/**
 *
 * @author Hossam ElDeen
 */
public interface Message {
    public void setDate(Date date);
    public void setMessageContent(String content);
    public void setDeleted(Boolean deleted);
    public void setFromUser(User fromUser);
    public Date getDate();
    public String getMessageContent();
    public Boolean getDeleted();
    public User getFromUser();
}
