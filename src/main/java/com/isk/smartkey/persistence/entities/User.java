/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Hossam ElDeen
 */
@Entity
@Table(name = "users")
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "unique_id")
    private String uniqueId;
    @Basic(optional = false)
    @Column(name = "first_name")
    private String firstName;
    @Basic(optional = false)
    @Column(name = "last_name")
    private String lastName;
    @Basic(optional = false)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @Column(name = "phone")
    private String phone;
    @Basic(optional = false)
    @Column(name = "encrypted_password")
    private String encryptedPassword;
    @Basic(optional = false)
    @Column(name = "salt")
    private String salt;
    @Basic(optional = false)
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;
    @Basic(optional = false)
    @Column(name = "picture")
    private String picture;
    @Basic(optional = false)
    @Column(name = "deleted")
    private boolean deleted;
    @Basic(optional = true)
    @Column(name = "last_login")
    private Date lastLogin;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fromUser")
    private List<GroupMessage> groupMessagesSentList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fromUser")
    private List<UserMessage> userMessagesSentList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "toUser")
    private List<UserMessage> userMessagesReceivedList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<UserGroupLink> userGroupLinksList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user")
    private List<UserLog> userLogsList;

    public User() {
    }

    public User(String uniqueId, String firstName, String lastName, String email, String phone, String encryptedPassword, String salt, Date createdAt, Date updatedAt, String picture) {
        this.uniqueId = uniqueId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.encryptedPassword = encryptedPassword;
        this.salt = salt;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.picture = picture;
    }

    public User(String uniqueId, String firstName, String lastName, String email, String phone, String encryptedPassword, String salt, Date createdAt, Date updatedAt, String picture, boolean deleted, List<GroupMessage> groupMessagesSentList, List<UserMessage> userMessagesSentList, List<UserMessage> userMessagesReceivedList, List<UserGroupLink> userGroupLinksList, List<UserLog> userLogsList, Date lastLogin) {
        this.uniqueId = uniqueId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.encryptedPassword = encryptedPassword;
        this.salt = salt;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.picture = picture;
        this.deleted = deleted;
        this.groupMessagesSentList = groupMessagesSentList;
        this.userMessagesSentList = userMessagesSentList;
        this.userMessagesReceivedList = userMessagesReceivedList;
        this.userGroupLinksList = userGroupLinksList;
        this.userLogsList = userLogsList;
        this.lastLogin = lastLogin;
    }

    public User(Integer id, String uniqueId, String firstName, String lastName, String email, String phone, String encryptedPassword, String salt, Date createdAt, Date updatedAt, String picture, boolean deleted, List<GroupMessage> groupMessagesSentList, List<UserMessage> userMessagesSentList, List<UserMessage> userMessagesReceivedList, List<UserGroupLink> userGroupLinksList, List<UserLog> userLogsList, Date lastLogin) {
        this.id = id;
        this.uniqueId = uniqueId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.encryptedPassword = encryptedPassword;
        this.salt = salt;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.picture = picture;
        this.deleted = deleted;
        this.groupMessagesSentList = groupMessagesSentList;
        this.userMessagesSentList = userMessagesSentList;
        this.userMessagesReceivedList = userMessagesReceivedList;
        this.userGroupLinksList = userGroupLinksList;
        this.userLogsList = userLogsList;
        this.lastLogin = lastLogin;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public List<GroupMessage> getGroupMessagesSentList() {
        return groupMessagesSentList;
    }

    public void setGroupMessagesSentList(List<GroupMessage> groupMessagesSentList) {
        this.groupMessagesSentList = groupMessagesSentList;
    }

    public List<UserMessage> getUserMessagesSentList() {
        return userMessagesSentList;
    }

    public void setUserMessagesSentList(List<UserMessage> userMessagesSentList) {
        this.userMessagesSentList = userMessagesSentList;
    }

    public List<UserMessage> getUserMessagesReceivedList() {
        return userMessagesReceivedList;
    }

    public void setUserMessagesReceivedList(List<UserMessage> userMessagesReceivedList) {
        this.userMessagesReceivedList = userMessagesReceivedList;
    }

    public List<UserGroupLink> getUserGroupLinksList() {
        return userGroupLinksList;
    }

    public void setUserGroupLinksList(List<UserGroupLink> userGroupLinksList) {
        this.userGroupLinksList = userGroupLinksList;
    }

    public List<UserLog> getUserLogsList() {
        return userLogsList;
    }

    public void setUserLogsList(List<UserLog> userLogsList) {
        this.userLogsList = userLogsList;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }
    
    

    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "testgeneratebeanshibernateisk.Users[ id=" + id + " ]";
    }
    
}
