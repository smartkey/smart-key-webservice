/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.persistence.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Hossam ElDeen
 */
@Entity
@Table(name = "groups")
public class Group implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private String id;
    @Basic(optional = false)
    @Column(name = "secret_key")
    private String secretKey;
    @Basic(optional = false)
    @Column(name = "salt")
    private String salt;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Column(name = "deleted")
    private Boolean deleted;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "toGroup")
    private List<GroupMessage> groupMessagesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "group")
    private List<BatteryLog> batteryLogsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "group")
    private List<UserGroupLink> userGroupLinksList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "group")
    private List<UserLog> userLogsList;
    
    
    
    public Group() {
    }

    public Group(String secretKey, String salt) {
        this.secretKey = secretKey;
        this.salt = salt;
    }

    public Group(String secretKey, String salt, Boolean deleted, List<GroupMessage> groupMessagesList, List<BatteryLog> batteryLogsList, List<UserGroupLink> userGroupLinksList, List<UserLog> userLogsList, String name) {
        this.secretKey = secretKey;
        this.salt = salt;
        this.deleted = deleted;
        this.groupMessagesList = groupMessagesList;
        this.batteryLogsList = batteryLogsList;
        this.userGroupLinksList = userGroupLinksList;
        this.userLogsList = userLogsList;
        this.name = name;
    }

    public Group(String id, String secretKey, String salt, Boolean deleted, List<GroupMessage> groupMessagesList, List<BatteryLog> batteryLogsList, List<UserGroupLink> userGroupLinksList, List<UserLog> userLogsList, String name) {
        this.id = id;
        this.secretKey = secretKey;
        this.salt = salt;
        this.deleted = deleted;
        this.groupMessagesList = groupMessagesList;
        this.batteryLogsList = batteryLogsList;
        this.userGroupLinksList = userGroupLinksList;
        this.userLogsList = userLogsList;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public List<GroupMessage> getGroupMessagesList() {
        return groupMessagesList;
    }

    public void setGroupMessagesList(List<GroupMessage> groupMessagesList) {
        this.groupMessagesList = groupMessagesList;
    }

    public List<BatteryLog> getBatteryLogsList() {
        return batteryLogsList;
    }

    public void setBatteryLogsList(List<BatteryLog> batteryLogsList) {
        this.batteryLogsList = batteryLogsList;
    }

    public List<UserGroupLink> getUserGroupLinksList() {
        return userGroupLinksList;
    }

    public void setUserGroupLinksList(List<UserGroupLink> userGroupLinksList) {
        this.userGroupLinksList = userGroupLinksList;
    }

    public List<UserLog> getUserLogsList() {
        return userLogsList;
    }

    public void setUserLogsList(List<UserLog> userLogsList) {
        this.userLogsList = userLogsList;
    }

    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Group)) {
            return false;
        }
        Group other = (Group) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "testgeneratebeanshibernateisk.Groups[ id=" + id + " ]";
    }
    
}
