/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Hossam ElDeen
 */
@Entity
@Table(name = "users_has_groups")
public class UserGroupLink implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserGroupLinkPK userGroupLinkPK;
    
    
    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    
    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    
    @Column(name = "start_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;
    
    @Column(name = "end_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endTime;
    
    @Column(name = "days")
    private String days;
    
    @Column(name = "deleted")
    private Boolean deleted;
    
    @JoinColumn(name = "groups_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Group group;
    
    @JoinColumn(name = "Permissions_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Permission permission;
    
    @JoinColumn(name = "users_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private User user;

    public UserGroupLink() {
    }

    public UserGroupLink(UserGroupLinkPK userGroupLinkPK, Group group, Permission permission, User user) {
        this.userGroupLinkPK = userGroupLinkPK;
        this.group = group;
        this.permission = permission;
        this.user = user;
    }

    public UserGroupLink(UserGroupLinkPK userGroupLinkPK, 
            Date startDate, Date endDate, Date startTime, Date endTime, String days, Boolean deleted, Group group, Permission permission, User user) {
        this.userGroupLinkPK = userGroupLinkPK;
        this.deleted = deleted;
        this.group = group;
        this.permission = permission;
        this.user = user;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.days = days;
    }

    public UserGroupLinkPK getUserGroupLinkPK() {
        return userGroupLinkPK;
    }

    public void setUserGroupLinkPK(UserGroupLinkPK userGroupLinkPK) {
        this.userGroupLinkPK = userGroupLinkPK;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }
    
    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Permission getPermission() {
        return permission;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getUserGroupLinkPK() != null ? getUserGroupLinkPK().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserGroupLink)) {
            return false;
        }
        UserGroupLink other = (UserGroupLink) object;
        if ((this.getUserGroupLinkPK() == null && other.getUserGroupLinkPK() != null) || (this.getUserGroupLinkPK() != null && !this.getUserGroupLinkPK().equals(other.getUserGroupLinkPK()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "testgeneratebeanshibernateisk.UsersHasGroups[ usersHasGroupsPK=" + getUserGroupLinkPK() + " ]";
    }
    
}
