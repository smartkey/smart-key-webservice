/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.persistence.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Hossam ElDeen
 */
@Entity
@Table(name = "permissions")
public class Permission implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @Column(name = "manage_users")
    private boolean manageUsers;
    
    @Basic(optional = false)
    @Column(name = "log_view")
    private boolean logView;
    
    @Basic(optional = false)
    @Column(name = "pin_message")
    private boolean pinMessage;
    
    @Basic(optional = false)
    @Column(name = "battery")
    private boolean battery;
    
    @Column(name = "deleted")
    private Boolean deleted;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "permission")
    private List<UserGroupLink> userGroupLinksList;

    public Permission() {
    }

    public Permission(boolean manageUsers, boolean logView, boolean pinMessage, boolean battery) {
        this.manageUsers = manageUsers;
        this.logView = logView;
        this.pinMessage = pinMessage;
        this.battery = battery;
    }

    public Permission(Integer id, boolean manageUsers, boolean logView, boolean pinMessage, boolean battery, Boolean deleted, List<UserGroupLink> userGroupLinksList) {
        this.id = id;
        this.manageUsers = manageUsers;
        this.logView = logView;
        this.pinMessage = pinMessage;
        this.battery = battery;
        this.deleted = deleted;
        this.userGroupLinksList = userGroupLinksList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public boolean isManageUsers() {
        return manageUsers;
    }

    public void setManageUsers(boolean manageUsers) {
        this.manageUsers = manageUsers;
    }

    public boolean isLogView() {
        return logView;
    }

    public void setLogView(boolean logView) {
        this.logView = logView;
    }

    public boolean isPinMessage() {
        return pinMessage;
    }

    public void setPinMessage(boolean pinMessage) {
        this.pinMessage = pinMessage;
    }

    public boolean isBattery() {
        return battery;
    }

    public void setBattery(boolean battery) {
        this.battery = battery;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public List<UserGroupLink> getUserGroupLinksList() {
        return userGroupLinksList;
    }

    public void setUserGroupLinksList(List<UserGroupLink> userGroupLinksList) {
        this.userGroupLinksList = userGroupLinksList;
    }

    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Permission)) {
            return false;
        }
        Permission other = (Permission) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "testgeneratebeanshibernateisk.Permissions[ id=" + id + " ]";
    }
    
}
