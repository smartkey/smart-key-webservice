/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.persistence.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Hossam ElDeen
 */
@Entity
@Table(name = "group_messages")
public class GroupMessage implements Serializable,Message{
    private static final long serialVersionUID = 1L;
    
    @Basic(optional = false)
    @Lob
    @Column(name = "message_content")
    private String messageContent;
    
    @Basic(optional = false)
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    
    @Column(name = "deleted")
    private Boolean deleted;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @JoinColumn(name = "to_group", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Group toGroup;

    @JoinColumn(name = "from_user", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private User fromUser;
    
    
    public GroupMessage() {
    }

    public GroupMessage(String messageContent, Date date, Group toGroup,User fromUser) {
        this.messageContent = messageContent;
        this.date = date;
        this.toGroup = toGroup;
        this.fromUser= fromUser;
    }

    public GroupMessage(Integer id, String messageContent, Date date, Boolean deleted, Group toGroup, User fromUser) {
        this.messageContent = messageContent;
        this.date = date;
        this.deleted = deleted;
        this.id = id;
        this.toGroup = toGroup;
        this.fromUser = fromUser;
    }
    
    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public Group getToGroup() {
        return toGroup;
    }

    public void setToGroup(Group toGroup) {
        this.toGroup = toGroup;
    }

    public User getFromUser() {
        return fromUser;
    }

    public void setFromUser(User fromUser) {
        this.fromUser = fromUser;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GroupMessage)) {
            return false;
        }
        GroupMessage other = (GroupMessage) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "testgeneratebeanshibernateisk.GroupMessages[ id=" + getId() + " ]";
    }
    
}
