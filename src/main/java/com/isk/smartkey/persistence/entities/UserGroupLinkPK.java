/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Hossam ElDeen
 */
@Embeddable
public class UserGroupLinkPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "users_id")
    private int userId;
    @Basic(optional = false)
    @Column(name = "groups_id")
    private String groupId;

    public UserGroupLinkPK() {
    }

    public UserGroupLinkPK(int userId, String groupId) {
        this.userId = userId;
        this.groupId = groupId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) userId;
        hash += (int) groupId.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserGroupLinkPK)) {
            return false;
        }
        UserGroupLinkPK other = (UserGroupLinkPK) object;
        if (this.userId != other.userId) {
            return false;
        }
        if (this.groupId != other.groupId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "testgeneratebeanshibernateisk.UsersHasGroupsPK[ usersId=" + userId + ", groupsId=" + groupId + " ]";
    }
    
}
