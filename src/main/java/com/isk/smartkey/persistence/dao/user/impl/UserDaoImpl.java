/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.persistence.dao.user.impl;

import com.isk.smartkey.business.common.dto.businessResponse.ServiceStatusDTO;
import com.isk.smartkey.business.group.dto.user.UserDTO;
import com.isk.smartkey.business.login.dto.user.UserLoginDto;
import com.isk.smartkey.business.registeration.util.PasswordHash;
import com.isk.smartkey.exception.types.SmartKeyPersistenceException;
import com.isk.smartkey.persistence.dao.user.UserDao;
import com.isk.smartkey.persistence.entities.Group;
import com.isk.smartkey.persistence.entities.User;
import com.isk.smartkey.persistence.entities.UserGroupLink;
import com.isk.smartkey.persistence.entities.UserGroupLinkPK;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Hossam ElDeen
 */
@Primary
@Repository
public class UserDaoImpl extends UserDao {

    @Autowired
    public UserDaoImpl(SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }

    
    @Transactional
    @Override
    public User load(int id){
        User user = (User) super.load(User.class, id);
        Hibernate.initialize(user.getUserGroupLinksList());
        return user;
    }
    
    /**
     *
     * @param email
     *
     * @return user if there is a one with the given email or null if there is
     * no user with that email
     */
    @Transactional
    @Override
    public User getUserByEmail(String email) {
        if (email == null) {
            throw new IllegalArgumentException("email can not be null");
        }

        Criteria cr = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(User.class);
        cr.add(Restrictions.eq("email", email));
        cr.add(Restrictions.eq("deleted", false));
        List result = cr.list();

        if (result != null && result.size() > 0) {
            return (User) result.get(0);
        } else {
            return null;
        }
    }

    /**
     *
     * @param email
     *
     * @return true if user exists or false if user does not exist
     */
    @Transactional
    @Override
    public boolean isUserExist(String email) {
        if (email == null) {
            throw new IllegalArgumentException("email can not be null");
        }

        Criteria cr = getSessionFactory().getCurrentSession().createCriteria(User.class);
        cr.add(Restrictions.eq("email", email));
        cr.add(Restrictions.eq("deleted", false));
        List result = cr.list();

        if (result != null && result.size() > 0) {
            return true;
        } else {
            return false;
        }

    }

    @Transactional
    @Override
    public boolean isUserExistWithTheSamePhone(String phone) {
        if (phone == null) {
            throw new IllegalArgumentException("email can not be null");
        }

        Criteria cr = getSessionFactory().getCurrentSession().createCriteria(User.class);
        cr.add(Restrictions.eq("phone", phone));
        cr.add(Restrictions.eq("deleted", false));
        List result = cr.list();

        if (result != null && result.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param email
     * @param password
     *
     * @return true if user is authenticated, or false if user is not
     * authenticated
     */
    @Transactional
    @Override
    public boolean isAuthenticatedUser(String email, String password) {
        if (email == null) {
            throw new IllegalArgumentException("email can not be null");
        } else if (password == null) {
            throw new IllegalArgumentException("password can not be null");
        }

        Criteria cr = getSessionFactory().getCurrentSession().createCriteria(User.class);
        cr.add(Restrictions.eq("email", email));
        cr.add(Restrictions.eq("encryptedPassword", password));
        cr.add(Restrictions.eq("deleted", false));
        List result = cr.list();

        if (result != null && result.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param email
     * @param password
     *
     * @return user object if authenticated or null if it is not authenticated
     */
    @Transactional
    @Override
    public UserLoginDto getUserDataIfAuthenticated(String email, String password) {
        if (email == null) {
            throw new IllegalArgumentException("email can not be null");
        } else if (password == null) {
            throw new IllegalArgumentException("password can not be null");
        }

        Criteria cr = getSessionFactory().getCurrentSession().createCriteria(User.class);
        cr.add(Restrictions.eq("email", email));
        cr.add(Restrictions.eq("deleted", false));
        List result = cr.list();

        if (result != null && result.size() > 0) {
            User user = (User) result.get(0);
            UserLoginDto userLoginDto = new UserLoginDto(user);
            try {
                if(PasswordHash.validatePassword(password,user.getSalt() ,user.getEncryptedPassword()))
                    return userLoginDto;
                else
                    return null;
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(UserDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            } catch (InvalidKeySpecException ex) {
                Logger.getLogger(UserDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }
        } else {
            return null;
        }
    }

    @Transactional
    @Override
    public boolean isAuthenticatedUser(int id, String password) {
        if (password == null) {
            throw new IllegalArgumentException("password can not be null");
        }

        Criteria cr = getSessionFactory().getCurrentSession().createCriteria(User.class);
        cr.add(Restrictions.eq("id", id));
        cr.add(Restrictions.eq("encryptedPassword", password));
        cr.add(Restrictions.eq("deleted", false));
        List result = cr.list();

        if (result != null && result.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    @Override
    //FIXME
    public List<UserDTO> getUsersOnGroup(Group group) {
        if(group == null){
            throw new IllegalArgumentException("group can not be null");
        }
        
        Criteria cr = getSessionFactory().getCurrentSession().createCriteria(UserGroupLink.class);
        cr.setProjection(Projections.property("user"));
        cr.add(Restrictions.eq("group", group));
        cr.add(Restrictions.eq("deleted", false));
        List<User> result = cr.list();

        if (result != null && result.size() > 0) {
            List<UserDTO> userOfGroupDTOs = UserDTO.copyList(result);
            return userOfGroupDTOs;
        } else {
            return null;
        }
    }

    @Transactional
    @Override
    public UserGroupLink loadUserGroupLink(User user, Group group) {
        if(user==null || group ==null){
            throw new IllegalArgumentException("user or group can not be null");
        }
        UserGroupLinkPK id = new UserGroupLinkPK(user.getId(), group.getId());
        
        try{
            UserGroupLink userGroupLink = getHibernateTemplate().get(UserGroupLink.class,id);
            return userGroupLink;
        }catch(RuntimeException ex){
            throw new SmartKeyPersistenceException("unable to load an object of class UserGroupLink because \n"+ex.toString());
        }
        
    }

    @Transactional
    @Override
    public UserLoginDto getUserDataByEmail(String email) {
        User user = getUserByEmail(email);
        UserLoginDto userLoginDto = new UserLoginDto(user);
        return userLoginDto;
    }

    
    @Transactional
    @Override
    public void createUserCred(String NativeQuery) {
        SQLQuery sqlQuery = getHibernateTemplate().getSessionFactory().getCurrentSession().createSQLQuery(NativeQuery);
        sqlQuery.executeUpdate();
    }
    
    @Override
    @Transactional
    public Object loadUserWithGroupsInitializedViaEmail(String userToBeAddedEmail) {
        User user;
        try {
            Criteria cr = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(User.class);
            cr.add(Restrictions.eq("email", userToBeAddedEmail));
            user = (User) cr.uniqueResult();
        } catch (Exception ex) {
            return new ServiceStatusDTO("FAILED", "an internal error has been happened.");
        }

        if (user == null) {
            return new ServiceStatusDTO("FAILED", "user does not exist.");
        } else {
            Hibernate.initialize(user.getUserGroupLinksList());
            return user;
        }
    }
    
    @Override
    @Transactional
    public Object loadUserWithGroupsInitializedViaPhone(String userToBeAddedPhone) {
        User user;
        try {
            Criteria cr = getHibernateTemplate().getSessionFactory().getCurrentSession().createCriteria(User.class);
            cr.add(Restrictions.eq("phone", userToBeAddedPhone));
            user = (User) cr.uniqueResult();
        } catch (Exception ex) {
            return new ServiceStatusDTO("FAILED", "an internal error has been happened.");
        }

        if (user == null) {
            return new ServiceStatusDTO("FAILED", "user does not exist.");
        } else {
            Hibernate.initialize(user.getUserGroupLinksList());
            return user;
        }
    }

    @Override
    @Transactional
    public void updateLoggingTimeOfUser(String email) {
        User user = getUserByEmail(email);
        user.setLastLogin(new Date());
        update(user);
    }
}
