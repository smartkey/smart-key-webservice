/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.persistence.dao.permission;

import com.isk.smartkey.persistence.dao.SmartKeyDao;
import com.isk.smartkey.persistence.entities.Group;
import com.isk.smartkey.persistence.entities.Permission;
import com.isk.smartkey.persistence.entities.User;
import java.util.List;

/**
 *
 * @author Hossam ElDeen
 */
public abstract class PermissionDao extends SmartKeyDao{
    public abstract Permission load(int id);
    public abstract Permission getPermissionOfUserOnGroup(Group group, User user);
}
