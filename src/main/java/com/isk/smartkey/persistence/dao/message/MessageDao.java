/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.persistence.dao.message;

import com.isk.smartkey.business.message.dto.Messages.MessageResponseDTO;
import com.isk.smartkey.persistence.dao.SmartKeyDao;
import com.isk.smartkey.persistence.entities.Group;
import com.isk.smartkey.persistence.entities.User;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Hossam ElDeen
 */
public abstract class MessageDao extends SmartKeyDao{
    public abstract List<MessageResponseDTO> getMessagesToGroup(Group group);
    public abstract List<MessageResponseDTO> getMessagesToUser(User user);

   // public abstract List<MessageResponseDTO> getAllMessages(Group group, User user);

    public abstract List<MessageResponseDTO> getAllMessages(Group group, User user, Date date) ;
}
