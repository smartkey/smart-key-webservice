/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.persistence.dao.message.impl;

import com.isk.smartkey.business.message.dto.Messages.MessageResponseDTO;
import com.isk.smartkey.business.message.impl.MessageControllerImpl;
import com.isk.smartkey.persistence.dao.message.MessageDao;
import com.isk.smartkey.persistence.entities.Group;
import com.isk.smartkey.persistence.entities.GroupMessage;
import com.isk.smartkey.persistence.entities.User;
import com.isk.smartkey.persistence.entities.UserMessage;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Hossam ElDeen
 */
@Repository
public class MessageDaoImpl extends MessageDao {

    @Autowired
    public MessageDaoImpl(SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }

    @Transactional
    @Override
    public List<MessageResponseDTO> getMessagesToGroup(Group group) {
        if (group == null) {
            throw new IllegalArgumentException("group can not be null");
        }

        Criteria cr = getSessionFactory().getCurrentSession().createCriteria(GroupMessage.class);
        cr.add(Restrictions.eq("toGroup", group));
        cr.add(Restrictions.eq("deleted", false));
        cr.addOrder(Order.desc("date"));
        List<GroupMessage> result = cr.list();

        if (result != null && result.size() > 0) {
            List<MessageResponseDTO> messageResponseDTOs = MessageResponseDTO.copy(result);
            return messageResponseDTOs;
        } else {
            return null;
        }
    }

    @Transactional
    @Override
    public List<MessageResponseDTO> getMessagesToUser(User user) {
        if (user == null) {
            throw new IllegalArgumentException("user can not be null");
        }

        Criteria cr = getSessionFactory().getCurrentSession().createCriteria(UserMessage.class);
        cr.add(Restrictions.eq("toUser", user));
        cr.add(Restrictions.eq("deleted", false));
        cr.addOrder(Order.desc("date"));
        List<UserMessage> result = cr.list();

        if (result != null && result.size() > 0) {
            List<MessageResponseDTO> messageResponseDTOs = MessageResponseDTO.copy(result);
            return messageResponseDTOs;
        } else {
            return null;
        }
    }

    @Transactional
    @Override
    public List<MessageResponseDTO> getAllMessages(Group group, User user,Date date) {
        if (user == null || group == null) {
            throw new IllegalArgumentException("user or group can not be null");
        }

        List<MessageResponseDTO> MessageSentToUserList = this.getMessagesToUser(user);
        List<MessageResponseDTO> MessageSentToGroupList = this.getMessagesToGroup(group);

        List<MessageResponseDTO> AllMessages = new ArrayList<MessageResponseDTO>();
        if (MessageSentToUserList != null) {
            AllMessages.addAll(MessageSentToUserList);
        }
        if (MessageSentToGroupList != null) {
            AllMessages.addAll(MessageSentToGroupList);
        }

        
        Logger.getLogger(MessageDaoImpl.class).debug("found total messages with length = "+AllMessages.size());
        
        AllMessages = this.sortMessages(AllMessages);
        AllMessages = this.getLastTenMessages(AllMessages, date);
        
        
        if (AllMessages != null && AllMessages.size() > 0) {
            return AllMessages;
        } else {
            return null;
        }
    }
    
    private List<MessageResponseDTO> getLastTenMessages(List<MessageResponseDTO> messages,Date date){
        List<MessageResponseDTO> filteredList = new ArrayList<MessageResponseDTO>();
        
        Integer indexOfLastSentMessage = getIndexOfMessageWithCertainDate(messages, date) ; 
        if(indexOfLastSentMessage == null)
            return filteredList;
        
        int count = 0 ;
        //increment to get the next message and skip the alreadu sent one
//        indexOfLastSentMessage++;
        
        while(count <10 && indexOfLastSentMessage < messages.size()){
            filteredList.add(messages.get(indexOfLastSentMessage));
            indexOfLastSentMessage++;
            count++;
        }
        
        return filteredList;
    }
    
    private List<MessageResponseDTO> sortMessages(List<MessageResponseDTO> messages) {
        int index;
        Date temp;
        List<MessageResponseDTO> sortedList = new ArrayList<MessageResponseDTO>();
        
        
        int size = messages.size() ;
        
        
        for (int i = 0; i < size; i++) {
            index = 0 ;
            temp = messages.get(index).getDate();
            for (int j = 1; j < messages.size(); j++) {
                if (messages.get(j).getDate().after(temp)) {
                    index = j;
                    temp = messages.get(j).getDate();
                }
            }
            sortedList.add(messages.get(index));
            
            messages.remove(index);
        }
        return sortedList;
    }
    
    
    private Integer getIndexOfMessageWithCertainDate(List<MessageResponseDTO> messages, Date date){
        for(int i=0; i<messages.size(); i++){
            
            if(date.after(messages.get(i).getDate()))
                return i;
        }
        return null;
    }
    
    private boolean isDatesEqual(Date date1, Date date2){
        if( date1.getMinutes() == date2.getMinutes()
                &&date1.getHours() == date2.getHours()
                &&date1.getSeconds() == date2.getSeconds()
                &&date1.getDay() == date2.getDay()
                &&date1.getMonth() == date2.getMonth()
                &&date1.getYear() == date2.getYear()){
            return true;
        }else
            return false;
    }
}
