/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.persistence.dao.batteryLog;

import com.isk.smartkey.business.batteryLog.dto.batteryLog.BatteryLogViewDTO;
import com.isk.smartkey.persistence.dao.SmartKeyDao;
import com.isk.smartkey.persistence.entities.Group;
import java.util.List;

/**
 *
 * @author Hossam ElDeen
 */
public abstract class BatteryLogDao extends SmartKeyDao{

    public abstract List<BatteryLogViewDTO> getLogsOfGroup(Group group);
    
}
