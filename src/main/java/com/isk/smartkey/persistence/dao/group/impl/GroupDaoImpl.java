/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.persistence.dao.group.impl;

import com.isk.smartkey.persistence.dao.group.GroupDao;
import com.isk.smartkey.persistence.entities.Group;
import com.isk.smartkey.persistence.entities.Permission;
import com.isk.smartkey.persistence.entities.User;
import com.isk.smartkey.persistence.entities.UserGroupLink;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Hossam ElDeen
 */
@Repository
public class GroupDaoImpl extends GroupDao {

    @Autowired
    public GroupDaoImpl(SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }

    @Transactional
    @Override
    public UserGroupLink getUserHasGroup(Group group, User user) {
        Criteria cr = getSessionFactory().getCurrentSession().createCriteria(UserGroupLink.class);
        cr.add(Restrictions.eq("group", group));
        cr.add(Restrictions.eq("user", user));
        cr.add(Restrictions.eq("deleted", false));
        List result = cr.list();
        
        if (result != null && result.size() > 0) {
            return (UserGroupLink) result.get(0);
        } else {
            return null;
        }
    }

    @Transactional
    @Override
    public Group load(int id) {
        Group group = (Group) super.load(Group.class, id);
        Hibernate.initialize(group.getUserGroupLinksList());
        return group;
    }

    @Override
    public void updateGroup(Group group) {
        getHibernateTemplate().saveOrUpdate(group);
    }

}