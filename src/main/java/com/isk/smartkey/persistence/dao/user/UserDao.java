/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.persistence.dao.user;

import com.isk.smartkey.business.group.dto.user.UserDTO;
import com.isk.smartkey.business.login.dto.user.UserLoginDto;
import com.isk.smartkey.persistence.dao.SmartKeyDao;
import com.isk.smartkey.persistence.dao.SmartKeyDao;
import com.isk.smartkey.persistence.entities.Group;
import com.isk.smartkey.persistence.entities.User;
import com.isk.smartkey.persistence.entities.UserGroupLink;
import com.isk.smartkey.persistence.entities.UserGroupLinkPK;
import java.util.List;

/**
 *
 * @author Hossam ElDeen
 */
public abstract class UserDao extends SmartKeyDao{
    public abstract UserGroupLink loadUserGroupLink(User user, Group group);
    
    public abstract User load(int id);
    /**
     * 
     * @param email
     * 
     * @return user if there is a one with the given email 
     *          or null if there is no user with that email
     */
    public abstract User getUserByEmail(String email);
    
    public abstract UserLoginDto getUserDataByEmail(String email);
    /**
     * 
     * @param email
     * 
     * @return true if user exists or false if user does not exist
     */
    public abstract boolean isUserExist(String email);
    /**
     * 
     * @param email
     * @param password
     * 
     * @return  true if user is authenticated, or false if user is not authenticated
     */
    public abstract boolean isAuthenticatedUser(String email, String password);
    /**
     * 
     * @param email
     * @param password
     * 
     * @return  true if user is authenticated, or false if user is not authenticated
     */
    public abstract boolean isAuthenticatedUser(int id, String password);
    /**
     * 
     * @param email
     * @param password
     * 
     * @return user object if authenticated or null if it is not authenticated
     */
    public abstract UserLoginDto getUserDataIfAuthenticated(String email, String password);
    /**
     * 
     * @param phone
     * @return  true if there is a user with the same phone
     */
    public abstract boolean isUserExistWithTheSamePhone(String phone);

    public abstract List<UserDTO> getUsersOnGroup(Group group);

    public abstract void createUserCred(String NativeQuery);
    
    public abstract Object loadUserWithGroupsInitializedViaEmail(String userToBeAddedEmail);
    public abstract Object loadUserWithGroupsInitializedViaPhone(String userToBeAddedEmail);

    public abstract void updateLoggingTimeOfUser(String email) ;
}
