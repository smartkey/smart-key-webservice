/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.persistence.dao.group;

import com.isk.smartkey.persistence.dao.SmartKeyDao;
import com.isk.smartkey.persistence.entities.Group;
import com.isk.smartkey.persistence.entities.Permission;
import com.isk.smartkey.persistence.entities.User;
import com.isk.smartkey.persistence.entities.UserGroupLink;

/**
 *
 * @author Hossam ElDeen
 */
public abstract class GroupDao extends SmartKeyDao{
    public abstract Group load(int id);
    public abstract UserGroupLink getUserHasGroup(Group group,User user);
    public abstract void updateGroup(Group group);
}
