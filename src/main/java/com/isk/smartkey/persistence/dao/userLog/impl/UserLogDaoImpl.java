/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.persistence.dao.userLog.impl;

import com.isk.smartkey.business.group.dto.user.UserDTO;
import com.isk.smartkey.business.userLog.dto.userLog.UserLogViewDTO;
import com.isk.smartkey.persistence.dao.userLog.UserLogDao;
import com.isk.smartkey.persistence.entities.Group;
import com.isk.smartkey.persistence.entities.User;
import com.isk.smartkey.persistence.entities.UserGroupLink;
import com.isk.smartkey.persistence.entities.UserLog;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Hossam ElDeen
 */
@Repository
public class UserLogDaoImpl extends UserLogDao{

    @Autowired
    public UserLogDaoImpl(SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }

    @Transactional
    @Override
    public List<UserLogViewDTO> getLogsOfGroup(Group group) {
        if(group == null)
            throw new IllegalArgumentException("group can not be null.");
        
        Criteria cr = getSessionFactory().getCurrentSession().createCriteria(UserLog.class);
        cr.add(Restrictions.eq("group", group));
        cr.add(Restrictions.eq("deleted", false));
        List<UserLog> result = cr.list();

        if (result != null && result.size() > 0) {
            List<UserLogViewDTO> userLogViewDTOs = UserLogViewDTO.copy(result);
            return userLogViewDTOs;
        } else {
            return null;
        }
    }
    
    
    
}
