/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.persistence.dao.permission.impl;

import com.isk.smartkey.persistence.dao.permission.PermissionDao;
import com.isk.smartkey.persistence.entities.Group;
import com.isk.smartkey.persistence.entities.Permission;
import com.isk.smartkey.persistence.entities.User;
import com.isk.smartkey.persistence.entities.UserGroupLink;
import java.util.Date;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Hossam ElDeen
 */
@Repository
public class PermissionDaoImpl extends PermissionDao {

    @Autowired
    public PermissionDaoImpl(SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }

    @Transactional
    @Override
    public Permission getPermissionOfUserOnGroup(Group group, User user) {
        Criteria cr = getSessionFactory().getCurrentSession().createCriteria(UserGroupLink.class);
        cr.setProjection(Projections.property("permission"));
        cr.add(Restrictions.eq("group", group));
        cr.add(Restrictions.eq("user", user));
        cr.add(Restrictions.eq("deleted", false));
        List result = cr.list();
        
        if (result != null && result.size() > 0) {
            Hibernate.initialize(((Permission)result.get(0)).isManageUsers());
            Hibernate.initialize(((Permission)result.get(0)).getUserGroupLinksList());
            return (Permission)result.get(0);
        } else {
            return null;
        }
    }

    @Transactional
    @Override
    public Permission load(int id) {
        Permission permission = (Permission) super.load(Permission.class, id);
        Hibernate.initialize(permission.getUserGroupLinksList());
        return permission;
    }
}
