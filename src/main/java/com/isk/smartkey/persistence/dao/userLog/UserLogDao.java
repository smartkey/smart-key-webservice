/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.persistence.dao.userLog;

import com.isk.smartkey.business.userLog.dto.userLog.UserLogViewDTO;
import com.isk.smartkey.persistence.dao.SmartKeyDao;
import com.isk.smartkey.persistence.entities.Group;
import java.util.List;

/**
 *
 * @author Hossam ElDeen
 */
public abstract class UserLogDao extends SmartKeyDao{

    public abstract List<UserLogViewDTO> getLogsOfGroup(Group group);
    
}
