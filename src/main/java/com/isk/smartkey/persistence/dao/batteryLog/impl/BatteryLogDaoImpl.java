/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.persistence.dao.batteryLog.impl;

import com.isk.smartkey.business.batteryLog.dto.batteryLog.BatteryLogViewDTO;
import com.isk.smartkey.business.userLog.dto.userLog.UserLogViewDTO;
import com.isk.smartkey.persistence.dao.batteryLog.BatteryLogDao;
import com.isk.smartkey.persistence.entities.BatteryLog;
import com.isk.smartkey.persistence.entities.Group;
import com.isk.smartkey.persistence.entities.UserLog;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Hossam ElDeen
 */
@Repository
public class BatteryLogDaoImpl extends BatteryLogDao{

    @Autowired
    public BatteryLogDaoImpl(SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }

    @Transactional
    @Override
    public List<BatteryLogViewDTO> getLogsOfGroup(Group group) {
        if(group == null)
            throw new IllegalArgumentException("group can not be null.");
        
        Criteria cr = getSessionFactory().getCurrentSession().createCriteria(BatteryLog.class);
        cr.add(Restrictions.eq("group", group));
        cr.add(Restrictions.eq("deleted", false));
        List<BatteryLog> result = cr.list();

        if (result != null && result.size() > 0) {
            List<BatteryLogViewDTO> batteryLogViewDTOs = BatteryLogViewDTO.copy(result);
            return batteryLogViewDTOs;
        } else {
            return null;
        }
    }
    
    
}
