/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.service.login;

import com.isk.smartkey.business.login.LoginController;
import com.isk.smartkey.business.login.impl.LoginControllerImpl;
import com.isk.smartkey.business.login.dto.user.UserCredentialsDto;
import com.isk.smartkey.business.login.dto.user.UserLoginDto;
import com.isk.smartkey.exception.SmartKeyException;
import com.isk.smartkey.persistence.dao.user.UserDao;
import com.isk.smartkey.persistence.dao.user.impl.UserDaoImpl;
import com.isk.smartkey.persistence.entities.User;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceDataWrapperDTO;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceStatusDTO;
import com.isk.smartkey.business.common.util.ServiceUtil;
import com.isk.smartkey.business.common.util.ValidationUtil;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hossam ElDeen
 */
@Service
@Path("/user/data")
public class LoginService {

    @Autowired(required = true)
    LoginController loginController;

    @Context
    private HttpServletRequest request;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public ServiceDataWrapperDTO login(@FormParam("email")String email) {
        
        Map argumentsMap = new HashMap<String,Object>();
        argumentsMap.put("email",email);
        String argumentsErrorMessage = ValidationUtil.checkExistenceOfArguments(argumentsMap);
        if(argumentsErrorMessage!= null)
            return ServiceUtil.transformToServiceDataWrapper(null,"FAILED", argumentsErrorMessage);
        
        UserCredentialsDto userCredentialsDto = new UserCredentialsDto(email, null);
        ServiceDataWrapperDTO serviceWrapper = loginController.login(userCredentialsDto);
        return serviceWrapper;
    }
    
}
