/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.service.message;

import com.isk.smartkey.business.common.dto.businessResponse.ServiceDataWrapperDTO;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceStatusDTO;
import com.isk.smartkey.business.common.util.ServiceUtil;
import com.isk.smartkey.business.common.util.ValidationUtil;
import com.isk.smartkey.business.message.MessageController;
import com.isk.smartkey.business.message.dto.Messages.GroupMessageSendingDTO;
import com.isk.smartkey.business.message.dto.Messages.MessageRetrievalDTO;
import com.isk.smartkey.business.message.dto.Messages.UserMessageSendingDTO;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hossam ElDeen
 */
@Service
@Path("/message")
public class MessageService {

    @Autowired(required = true)
    MessageController messageController;

    @Context
    private HttpServletRequest request;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/user/send")
    public ServiceStatusDTO sendMessageToUser(
            @FormParam("from_user_id") Integer fromUserId,
            @FormParam("to_user_id") Integer toUserId,
            @FormParam("date_time") Long dateTime,
            @FormParam("content") String content
    ) {
        Map argumentsMap = new HashMap<String, Object>();
        argumentsMap.put("from_user_id", fromUserId);
        argumentsMap.put("to_user_id", toUserId);
        argumentsMap.put("date_time", dateTime);
        argumentsMap.put("content", content);
        String argumentsErrorMessage = ValidationUtil.checkExistenceOfArguments(argumentsMap);
        if (argumentsErrorMessage != null) {
            return ServiceUtil.transformToServiceDataWrapper(null, "FAILED", argumentsErrorMessage);
        }

        Date date = new Date(dateTime);
        UserMessageSendingDTO userMessageSendingDTO = new UserMessageSendingDTO(fromUserId, toUserId, content, date);
        ServiceStatusDTO serviceStatusDTO = messageController.sendMessageToUser(userMessageSendingDTO);
        return serviceStatusDTO;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/user/get")
    public ServiceDataWrapperDTO getMessagesToUser(@FormParam("user_id") Integer userId) {

        Map argumentsMap = new HashMap<String, Object>();
        argumentsMap.put("user_id", userId);
        String argumentsErrorMessage = ValidationUtil.checkExistenceOfArguments(argumentsMap);
        if (argumentsErrorMessage != null) {
            return ServiceUtil.transformToServiceDataWrapper(null, "FAILED", argumentsErrorMessage);
        }

        MessageRetrievalDTO messageRetrievalDTO = new MessageRetrievalDTO(userId, null);
        ServiceDataWrapperDTO serviceDataWrapperDTO = messageController.getMessagesForUser(messageRetrievalDTO);
        return serviceDataWrapperDTO;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/group/send")
    public ServiceStatusDTO sendMessageToGroup(
            @FormParam("from_user_id") Integer fromUserId,
            @FormParam("to_group_id") String toGroupId,
            @FormParam("date_time") Long dateTime,
            @FormParam("content") String content
    ) {
        Map argumentsMap = new HashMap<String, Object>();
        argumentsMap.put("from_user_id", fromUserId);
        argumentsMap.put("to_group_id", toGroupId);
        argumentsMap.put("date_time", dateTime);
        argumentsMap.put("content", content);
        String argumentsErrorMessage = ValidationUtil.checkExistenceOfArguments(argumentsMap);
        if (argumentsErrorMessage != null) {
            return ServiceUtil.transformToServiceDataWrapper(null, "FAILED", argumentsErrorMessage);
        }

        Date date = new Date(dateTime);
        GroupMessageSendingDTO groupMessageSendingDTO = new GroupMessageSendingDTO(fromUserId, toGroupId, content, date);
        ServiceStatusDTO serviceStatusDTO = messageController.sendMessageToGroup(groupMessageSendingDTO);
        return serviceStatusDTO;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/group/get")
    public ServiceDataWrapperDTO getMessagesToGroup(
            @FormParam("user_id") Integer userId, 
            @FormParam("group_id") String groupId) {
        Map argumentsMap = new HashMap<String, Object>();
        argumentsMap.put("user_id", userId);
        argumentsMap.put("group_id", groupId);
        String argumentsErrorMessage = ValidationUtil.checkExistenceOfArguments(argumentsMap);
        if (argumentsErrorMessage != null) {
            return ServiceUtil.transformToServiceDataWrapper(null, "FAILED", argumentsErrorMessage);
        }

        MessageRetrievalDTO messageRetrievalDTO = new MessageRetrievalDTO(groupId, userId);
        ServiceDataWrapperDTO serviceDataWrapperDTO = messageController.getMessagesForGroup(messageRetrievalDTO);
        return serviceDataWrapperDTO;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getAll")
    public ServiceDataWrapperDTO getAllMessages(
            @FormParam("user_id") Integer userId,
            @FormParam("group_id") String groupId,
            @FormParam("date_time") Long dateTime
    ) {
        Map argumentsMap = new HashMap<String, Object>();
        argumentsMap.put("user_id", userId);
        argumentsMap.put("group_id", groupId);
        argumentsMap.put("date_time", dateTime);
        String argumentsErrorMessage = ValidationUtil.checkExistenceOfArguments(argumentsMap);
        if (argumentsErrorMessage != null) {
            return ServiceUtil.transformToServiceDataWrapper(null, "FAILED", argumentsErrorMessage);
        }
        
        Date date = new Date(dateTime);
        MessageRetrievalDTO messageRetrievalDTO = new MessageRetrievalDTO(groupId, userId, date);
        ServiceDataWrapperDTO serviceDataWrapperDTO = messageController.getAllMessages(messageRetrievalDTO);
        return serviceDataWrapperDTO;
    }
}
