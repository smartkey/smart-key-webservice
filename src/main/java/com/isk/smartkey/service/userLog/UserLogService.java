/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.service.userLog;

import com.isk.smartkey.business.common.dto.businessResponse.ServiceDataWrapperDTO;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceStatusDTO;
import com.isk.smartkey.business.common.util.ServiceUtil;
import com.isk.smartkey.business.common.util.ValidationUtil;
import com.isk.smartkey.business.userLog.UserLogController;
import com.isk.smartkey.business.userLog.dto.userLog.UserLogRetrievalDTO;
import com.isk.smartkey.business.userLog.dto.userLog.UserLogUpdateDTO;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hossam ElDeen
 */
@Service
@Path("/userLogs")
public class UserLogService {

    @Autowired(required = true)
    UserLogController userLogController;

    @Context
    private HttpServletRequest request;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/update")
    public ServiceStatusDTO update(
            @FormParam("user_id") Integer userId, 
            @FormParam("group_id") String groupId, 
            @FormParam("date_time") Long dateTime
    ) {
        Map argumentsMap = new HashMap<String, Object>();
        argumentsMap.put("user_id", userId);
        argumentsMap.put("group_id", groupId);
        argumentsMap.put("date_time", dateTime);
        String argumentsErrorMessage = ValidationUtil.checkExistenceOfArguments(argumentsMap);
        if (argumentsErrorMessage != null) {
            return ServiceUtil.transformToServiceDataWrapper(null, "FAILED", argumentsErrorMessage);
        }
        
        Date date = new Date(dateTime);
        UserLogUpdateDTO userLogUpdateDTO = new UserLogUpdateDTO(userId, groupId, date);
        ServiceStatusDTO serviceStatusDTO = userLogController.update(userLogUpdateDTO);

        return serviceStatusDTO;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/get")
    public ServiceDataWrapperDTO get(
            @FormParam("user_id") Integer userId, 
            @FormParam("group_id") String groupId) {
        Map argumentsMap = new HashMap<String, Object>();
        argumentsMap.put("user_id", userId);
        argumentsMap.put("group_id", groupId);
        String argumentsErrorMessage = ValidationUtil.checkExistenceOfArguments(argumentsMap);
        if (argumentsErrorMessage != null) {
            return ServiceUtil.transformToServiceDataWrapper(null, "FAILED", argumentsErrorMessage);
        }

        UserLogRetrievalDTO userLogRetrievalDTO = new UserLogRetrievalDTO(userId, groupId);
        ServiceDataWrapperDTO serviceStatusDTO = userLogController.get(userLogRetrievalDTO);

        return serviceStatusDTO;
    }
}
