/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.service.batteryLog;

import com.isk.smartkey.business.batteryLog.BatteryLogController;
import com.isk.smartkey.business.batteryLog.dto.batteryLog.BatteryLogRetrievalDTO;
import com.isk.smartkey.business.batteryLog.dto.batteryLog.BatteryLogUpdateDTO;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceDataWrapperDTO;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceStatusDTO;
import com.isk.smartkey.business.common.util.ServiceUtil;
import com.isk.smartkey.business.common.util.ValidationUtil;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hossam ElDeen
 */
@Service
@Path("/batteryLogs")
public class BatteryLogService {
    @Autowired(required = true)
    BatteryLogController batteryLogController;

    @Context
    private HttpServletRequest request;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/update")
    public ServiceStatusDTO update(
            @FormParam("battery_status") String batteryStatus, 
            @FormParam("group_id") String groupId,
            @FormParam("date_time") Long dateTime) {
        Map argumentsMap = new HashMap<String, Object>();
        argumentsMap.put("battery_status", batteryStatus);
        argumentsMap.put("group_id", groupId);
        argumentsMap.put("date_time", dateTime);
        String argumentsErrorMessage = ValidationUtil.checkExistenceOfArguments(argumentsMap);
        if (argumentsErrorMessage != null) {
            return ServiceUtil.transformToServiceDataWrapper(null, "FAILED", argumentsErrorMessage);
        }

        Date date = new Date(dateTime);
        BatteryLogUpdateDTO batteryLogUpdateDTO = new BatteryLogUpdateDTO(batteryStatus, groupId, date);
        ServiceStatusDTO serviceStatusDTO = batteryLogController.update(batteryLogUpdateDTO);

        return serviceStatusDTO;
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/get")
    public ServiceDataWrapperDTO get(
            @FormParam("group_id")String groupId,
            @FormParam("user_id")Integer userId) {
        Map argumentsMap = new HashMap<String, Object>();
        argumentsMap.put("group_id", groupId);
        argumentsMap.put("user_id", userId);
        String argumentsErrorMessage = ValidationUtil.checkExistenceOfArguments(argumentsMap);
        if (argumentsErrorMessage != null) {
            return ServiceUtil.transformToServiceDataWrapper(null, "FAILED", argumentsErrorMessage);
        }

        BatteryLogRetrievalDTO batteryLogRetrievalDTO = new BatteryLogRetrievalDTO(userId,groupId);
        ServiceDataWrapperDTO serviceDataWrapperDTO = batteryLogController.get(batteryLogRetrievalDTO);

        return serviceDataWrapperDTO;
    }
}
