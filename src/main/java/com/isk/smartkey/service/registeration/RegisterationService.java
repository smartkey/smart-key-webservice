/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.service.registeration;

import com.isk.smartkey.business.registeration.RegisterationController;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceDataWrapperDTO;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceStatusDTO;
import com.isk.smartkey.business.common.util.ValidationUtil;
import com.isk.smartkey.business.registeration.dto.user.UserRegisterDTO;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hossam ElDeen
 */
@Service
@Path("/register")
public class RegisterationService {

    @Autowired(required = true)
    RegisterationController registerationController;

    @Context
    private HttpServletRequest request;

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public ServiceStatusDTO register(
            @FormParam("first_name")String firstName,
            @FormParam("last_name")String lastName,
            @FormParam("email")String email,
            @FormParam("phone")String phone) {
        String password = request.getHeader("password");
        
        Map argumentsMap = new HashMap<String,Object>();
        argumentsMap.put("firstName",firstName);
        argumentsMap.put("lastName",lastName);
        argumentsMap.put("email",email);
        argumentsMap.put("phone",phone);
        argumentsMap.put("password",password);
        String argumentsErrorMessage = ValidationUtil.checkExistenceOfArguments(argumentsMap);
        if(argumentsErrorMessage!= null)
            return new ServiceStatusDTO("FAILED", argumentsErrorMessage);
        
        UserRegisterDTO userRegisterDTO = new UserRegisterDTO(firstName, lastName, email, phone, password);
        ServiceStatusDTO serviceStatusDTO = registerationController.registerUser(userRegisterDTO);
        
        return serviceStatusDTO;
    }

   
}
