/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.service.group;

import com.isk.smartkey.business.common.dto.businessResponse.ServiceDataWrapperDTO;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceStatusDTO;
import com.isk.smartkey.business.group.GroupController;
import com.isk.smartkey.business.group.dto.group.AddUserToGroupDTO;
import com.isk.smartkey.business.group.dto.group.DeleteUserFromGroupDTO;
import com.isk.smartkey.business.group.dto.group.GetUserOnGroupDTO;
import com.isk.smartkey.business.common.dto.group.GroupDTO;
import com.isk.smartkey.business.common.util.ServiceUtil;
import com.isk.smartkey.business.common.util.ValidationUtil;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hossam ElDeen
 */
@Service
@Path("/group")
public class GroupManipulationService {

    @Context
    private HttpServletRequest request;

    @Autowired(required = true)
    private GroupController groupController;

    @POST
    @Path("/create")
    @Produces(MediaType.APPLICATION_JSON)
    public ServiceStatusDTO createGroup(
            @FormParam("serial_number") String groupId,
            @FormParam("group_name") String groupName,
            @FormParam("secret_key") String secretKey,
            @FormParam("user_id") Integer userId) {

        Map argumentsMap = new HashMap<String, Object>();
        argumentsMap.put("serial_number", groupId);
        argumentsMap.put("group_name", groupName);
        argumentsMap.put("secret_key", secretKey);
        argumentsMap.put("user_id", userId);
        String argumentsErrorMessage = ValidationUtil.checkExistenceOfArguments(argumentsMap);
        if (argumentsErrorMessage != null) {
            return new ServiceStatusDTO("FAILED", argumentsErrorMessage);
        }

        GroupDTO groupCreationDTO = new GroupDTO(secretKey, groupName, groupId);
        ServiceStatusDTO serviceStatusDTO = groupController.createGroup(groupCreationDTO, userId);
        return serviceStatusDTO;
    }

//    @POST
//    @Path("/addUser")
//    @Produces(MediaType.APPLICATION_JSON)
//    public ServiceStatusDTO addUserToGroup(
//            @FormParam("group_id") String groupId,
//            @FormParam("user_id") Integer userId,
//            @FormParam("password") String password,
//            @FormParam("user_to_be_added_id") Integer userToBeAddedId,
//            @FormParam("removal_date_time") Long dateTime,
//            @FormParam("permission") Integer permissionId) {
//
//        Map argumentsMap = new HashMap<String, Object>();
//        argumentsMap.put("group_id", groupId);
//        argumentsMap.put("user_id", userId);
//        argumentsMap.put("password", password);
//        argumentsMap.put("user_to_be_added_id", userToBeAddedId);
//        argumentsMap.put("removal_date_time", dateTime);
//        argumentsMap.put("permission", permissionId);
//        String argumentsErrorMessage = ValidationUtil.checkExistenceOfArguments(argumentsMap);
//        if (argumentsErrorMessage != null) {
//            return new ServiceStatusDTO("FAILED", argumentsErrorMessage);
//        }
//
//        Date date = new Date(dateTime);
//        AddUserToGroupDTO addUserToGroupDTO = new AddUserToGroupDTO(groupId, userId, password, userToBeAddedId, date, permissionId);
//        ServiceStatusDTO addUserToGroupStatus = groupController.addUserToGroup(addUserToGroupDTO);
//        return addUserToGroupStatus;
//    }

    
    /*
    Existing parameters as usual:
user_id
group_id
email
password
permission --> 0 or 1 or 2 representing (Owner, Sub User, Guest)

New Parameters:
always --> boolean
all_time --> boolean
start_date_time --> long (since 1970)
end_date_time --> long (since 1970)
week_days --> String containing week days numbers in Calendar class (concatenated)
    
    */
    @POST
    @Path("/addUserWithEmail")
    @Produces(MediaType.APPLICATION_JSON)
    public ServiceStatusDTO addUserToGroup(
            @FormParam("group_id") String groupId,
            @FormParam("user_id") Integer userId,
            @FormParam("password") String password,
            @FormParam("email") String userToBeAddedEmail,
            @FormParam("always") Boolean always,
            @FormParam("all_time") Boolean allTime,
            @FormParam("start_date_time") Long start,
            @FormParam("end_date_time") Long end,
            @FormParam("week_days") String days,
            @FormParam("permission") Integer permissionId) {

        Map argumentsMap = new HashMap<String, Object>();
        argumentsMap.put("group_id", groupId);
        argumentsMap.put("user_id", userId);
        argumentsMap.put("password", password);
        argumentsMap.put("user_to_be_added_email", userToBeAddedEmail);
        argumentsMap.put("permission", permissionId);

        String argumentsErrorMessage = ValidationUtil.checkExistenceOfArguments(argumentsMap);
        if (argumentsErrorMessage != null) {
            return new ServiceStatusDTO("FAILED", argumentsErrorMessage);
        }
        
        Date startDate = ServiceUtil.prepareDates(start);
        Date endDate = ServiceUtil.prepareDates(end);
        days = ServiceUtil.prepareDays(days);
        
        Date startDateObj = new Date(startDate.getYear(), startDate.getMonth(), startDate.getDay(), 0, 0, 0);
        Date endDateObj = new Date(endDate.getYear(), endDate.getMonth(), endDate.getDay(), 0, 0, 0);
        Date startTimeObj = new Date(0, 0, 0, startDate.getHours(), startDate.getMinutes(), startDate.getSeconds());
        Date endTimeObj = new Date(0, 0, 0, startDate.getHours(), startDate.getMinutes(), startDate.getSeconds());
        
        if(always){
            startDateObj = null;
            endDateObj = null;
        }
        if(allTime){
            startTimeObj = null;
            endTimeObj = null;
        }
            
            
        AddUserToGroupDTO addUserToGroupDTO = new AddUserToGroupDTO(groupId, userId, password, userToBeAddedEmail, startDateObj, endDateObj, startTimeObj, endTimeObj, days, permissionId);
        ServiceStatusDTO addUserToGroupStatus = groupController.addUserToGroup(addUserToGroupDTO);
        return addUserToGroupStatus;
    }
    
    @POST
    @Path("/addUserWithEmailExt")
    @Produces(MediaType.APPLICATION_JSON)
    public ServiceStatusDTO addUserToGroupExt(
            @FormParam("group_id") String groupId,
            @FormParam("user_id") Integer userId,
            @FormParam("password") String password,
            @FormParam("email") String userToBeAddedEmail,
            @FormParam("start_date") Long startDate,
            @FormParam("end_date") Long endDate,
            @FormParam("start_time") Long startTime,
            @FormParam("end_time") Long endTime,
            @FormParam("days") String days,
            @FormParam("permission") Integer permissionId) {

        Map argumentsMap = new HashMap<String, Object>();
        argumentsMap.put("group_id", groupId);
        argumentsMap.put("user_id", userId);
        argumentsMap.put("password", password);
        argumentsMap.put("user_to_be_added_email", userToBeAddedEmail);
        argumentsMap.put("permission", permissionId);

        String argumentsErrorMessage = ValidationUtil.checkExistenceOfArguments(argumentsMap);
        if (argumentsErrorMessage != null) {
            return new ServiceStatusDTO("FAILED", argumentsErrorMessage);
        }

        Date startDateObj = ServiceUtil.prepareDates(startDate);
        Date endDateObj = ServiceUtil.prepareDates(endDate);
        Date startTimeObj = ServiceUtil.prepareDates(startTime);
        Date endTimeObj = ServiceUtil.prepareDates(endTime);
        days = ServiceUtil.prepareDays(days);

        AddUserToGroupDTO addUserToGroupDTO = new AddUserToGroupDTO(groupId, userId, password, userToBeAddedEmail, startDateObj, endDateObj, startTimeObj, endTimeObj, days, permissionId);
        ServiceStatusDTO addUserToGroupStatus = groupController.addUserToGroup(addUserToGroupDTO);
        return addUserToGroupStatus;
    }
//    @POST
//    @Path("/addUserWithPhone")
//    @Produces(MediaType.APPLICATION_JSON)
//    public ServiceStatusDTO addUserToGroupWithPhone(
//            @FormParam("group_id") String groupId,
//            @FormParam("user_id") Integer userId,
//            @FormParam("password") String password,
//            @FormParam("phone") String phone,
//            @FormParam("removal_date_time") Long dateTime,
//            @FormParam("permission") Integer permissionId) {
//
//        Map argumentsMap = new HashMap<String, Object>();
//        argumentsMap.put("group_id", groupId);
//        argumentsMap.put("user_id", userId);
//        argumentsMap.put("password", password);
//        argumentsMap.put("phone", phone);
//        argumentsMap.put("removal_date_time", dateTime);
//        argumentsMap.put("permission", permissionId);
//        String argumentsErrorMessage = ValidationUtil.checkExistenceOfArguments(argumentsMap);
//        if (argumentsErrorMessage != null) {
//            return new ServiceStatusDTO("FAILED", argumentsErrorMessage);
//        }
//
//        Date date = new Date(dateTime);
//        AddUserToGroupDTO addUserToGroupDTO = new AddUserToGroupDTO(groupId, userId, password, date, permissionId, phone);
//        ServiceStatusDTO addUserToGroupStatus = groupController.addUserToGroup(addUserToGroupDTO);
//        return addUserToGroupStatus;
//    }

    @POST
    @Path("/removeUser")
    @Produces(MediaType.APPLICATION_JSON)
    public ServiceStatusDTO deleteUserFromGroup(
            @FormParam("group_id") String groupId,
            @FormParam("user_id") Integer userId,
            @FormParam("password") String password,
            @FormParam("user_to_be_deleted_id") Integer userToBeDeletedId) {
        Map argumentsMap = new HashMap<String, Object>();
        argumentsMap.put("group_id", groupId);
        argumentsMap.put("user_id", userId);
        argumentsMap.put("password", password);
        argumentsMap.put("user_to_be_deleted_id", userToBeDeletedId);
        String argumentsErrorMessage = ValidationUtil.checkExistenceOfArguments(argumentsMap);
        if (argumentsErrorMessage != null) {
            return new ServiceStatusDTO("FAILED", argumentsErrorMessage);
        }

        DeleteUserFromGroupDTO deleteUserFromGroupDTO = new DeleteUserFromGroupDTO(groupId, userId, password, userToBeDeletedId);
        ServiceStatusDTO deleteUserFromGroupStatus = groupController.deleteUserFromGroup(deleteUserFromGroupDTO);
        return deleteUserFromGroupStatus;
    }

    @POST
    @Path("/getUsers")
    @Produces(MediaType.APPLICATION_JSON)
    public ServiceDataWrapperDTO getUsersOnGroup(
            @FormParam("group_id") String groupId,
            @FormParam("user_id") Integer userId) {
        Map argumentsMap = new HashMap<String, Object>();
        argumentsMap.put("group_id", groupId);
        argumentsMap.put("user_id", userId);
        String argumentsErrorMessage = ValidationUtil.checkExistenceOfArguments(argumentsMap);
        if (argumentsErrorMessage != null) {
            return ServiceUtil.transformToServiceDataWrapper(null, "FAILED", argumentsErrorMessage);
        }

        GetUserOnGroupDTO getUserOnGroupDTO = new GetUserOnGroupDTO(groupId, userId);
        ServiceDataWrapperDTO serviceDataWrapperDTO = groupController.getUsersOnGroup(getUserOnGroupDTO);
        return serviceDataWrapperDTO;
    }
}
