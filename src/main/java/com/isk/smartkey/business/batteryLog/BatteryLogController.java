/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.batteryLog;

import com.isk.smartkey.business.batteryLog.dto.batteryLog.BatteryLogRetrievalDTO;
import com.isk.smartkey.business.batteryLog.dto.batteryLog.BatteryLogUpdateDTO;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceDataWrapperDTO;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceStatusDTO;

/**
 *
 * @author Hossam ElDeen
 */
public interface BatteryLogController {
    public ServiceStatusDTO update(BatteryLogUpdateDTO userLogUpdateDTO);
    public ServiceDataWrapperDTO get(BatteryLogRetrievalDTO userLogRetrievalDTO);
}
