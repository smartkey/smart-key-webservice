/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.batteryLog.dto.batteryLog;

import com.isk.smartkey.business.common.serializer.CustomDateSerializer;
import com.isk.smartkey.business.userLog.dto.userLog.*;
import com.isk.smartkey.business.group.dto.user.UserDTO;
import com.isk.smartkey.persistence.entities.BatteryLog;
import com.isk.smartkey.persistence.entities.UserLog;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 *
 * @author Hossam ElDeen
 */
public class BatteryLogViewDTO {
    String batteryStatus;
    Date date;

    public BatteryLogViewDTO() {
    }

    public BatteryLogViewDTO(BatteryLog batteryLog){
        copy(batteryLog);
    }
    
    public BatteryLogViewDTO(String batteryStatus, Date date) {
        this.batteryStatus = batteryStatus;
        this.date = date;
    }

    public String getBatteryStatus() {
        return batteryStatus;
    }

    public void setBatteryStatus(String batteryStatus) {
        this.batteryStatus = batteryStatus;
    }

    @JsonSerialize(using = CustomDateSerializer.class)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    public final void copy(BatteryLog batteryLog){
        setDate(batteryLog.getDate());
        setBatteryStatus(batteryLog.getBatteryStatus());
    }
    
    public static List<BatteryLogViewDTO> copy(List<BatteryLog> batteryLogs){
        if(batteryLogs == null)
            return null;
        if(batteryLogs.size() < 1)
            return new ArrayList<BatteryLogViewDTO>();
        
        List<BatteryLogViewDTO> batteryLogViewDTOs = new ArrayList<BatteryLogViewDTO>();
        for(BatteryLog batteryLog: batteryLogs){
            BatteryLogViewDTO batteryLogViewDTO = new BatteryLogViewDTO(batteryLog);
            batteryLogViewDTOs.add(batteryLogViewDTO);
        }
        return batteryLogViewDTOs;
    }
}
