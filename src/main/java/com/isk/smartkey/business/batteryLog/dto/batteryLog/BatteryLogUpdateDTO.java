/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.batteryLog.dto.batteryLog;

import com.isk.smartkey.business.common.serializer.CustomDateSerializer;
import com.isk.smartkey.business.userLog.dto.userLog.*;
import com.isk.smartkey.persistence.entities.Group;
import com.isk.smartkey.persistence.entities.User;
import java.util.Date;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 *
 * @author Hossam ElDeen
 */
public class BatteryLogUpdateDTO {
    String batteryStatus;
    String groupId;
    Date date;

    public BatteryLogUpdateDTO() {
    }

    public BatteryLogUpdateDTO(String batteryStatus, String groupId, Date date) {
        this.batteryStatus = batteryStatus;
        this.groupId = groupId;
        this.date = date;
    }

    public String getBatteryStatus() {
        return batteryStatus;
    }

    public void setBatteryStatus(String batteryStatus) {
        this.batteryStatus = batteryStatus;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @JsonSerialize(using = CustomDateSerializer.class)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    
    
}
