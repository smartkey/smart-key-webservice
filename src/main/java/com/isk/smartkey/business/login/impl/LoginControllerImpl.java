/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.login.impl;

import com.isk.smartkey.business.login.LoginController;
import com.isk.smartkey.business.login.dto.user.UserCredentialsDto;
import com.isk.smartkey.business.login.dto.user.UserLoginDto;
import com.isk.smartkey.exception.SmartKeyException;
import com.isk.smartkey.exception.types.SmartKeyServiceExcpetion;
import com.isk.smartkey.persistence.dao.user.UserDao;
import com.isk.smartkey.persistence.entities.User;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceDataWrapperDTO;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceStatusDTO;
import com.isk.smartkey.business.common.util.DataBaseUtil;
import com.isk.smartkey.business.common.util.ServiceUtil;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author Hossam ElDeen
 */
@Controller
public class LoginControllerImpl implements LoginController {

    @Autowired(required = true)
    UserDao userDao;

    /**
     *
     * @param userCredentialsDto
     * @return user object if there is a user with the specified credentials or
     * null if there is not
     */
    private UserLoginDto getUserData(UserCredentialsDto userCredentialsDto) {
        if (userCredentialsDto == null) {
            throw new IllegalArgumentException("credentials of login are missing.");
        }
        UserLoginDto user = userDao.getUserDataByEmail(userCredentialsDto.getEmail());
        return user;
    }

    /**
     *
     * @param userCredentialsDto
     * @return UserLoginDto object that hold the appropriate data for the given
     * credentials
     */
    @Override
    public ServiceDataWrapperDTO login(UserCredentialsDto userCredentialsDto) {
        if (userCredentialsDto == null) {
            throw new IllegalArgumentException("UserCredentialsDto can not be null");
        }

        try {
            UserLoginDto user = getUserData(userCredentialsDto);
            updateLoggingTimeOfUser(userCredentialsDto);
            if (user == null) {
                return ServiceUtil.transformToServiceDataWrapper(null, "FAILED", "user does not exist.");
            } else {
                return ServiceUtil.transformToServiceDataWrapper(user, "SUCCESS", "user found with the attached data.");
            }
        } catch (IllegalArgumentException iae) {
            return handleIllegalArgumentException(iae.getMessage());
        } catch (Exception ex) {
            Logger.getLogger(LoginControllerImpl.class).error("an internal error has been happened", ex);
            return ServiceUtil.transformToServiceDataWrapper(null, "FAILED", "an internal error has been happened, please try again later.");
        }
    }

    private ServiceDataWrapperDTO handleIllegalArgumentException(String exceptionMessage) {
        if (exceptionMessage.startsWith("email")) {
            return ServiceUtil.transformToServiceDataWrapper(null, "FAILED", "email argument for login is missing.");
        } else if (exceptionMessage.startsWith("password")) {
            return ServiceUtil.transformToServiceDataWrapper(null, "FAILED", "password argument for login is missing.");
        } else if (exceptionMessage.startsWith("credentials")) {
            return ServiceUtil.transformToServiceDataWrapper(null, "FAILED", "credentials for login are missing.");
        } else {
            return ServiceUtil.transformToServiceDataWrapper(null, "FAILED", "an internal error has been happened, please try again later.");
        }
    }

    private void updateLoggingTimeOfUser(UserCredentialsDto userCredentialsDto) {
        userDao.updateLoggingTimeOfUser(userCredentialsDto.getEmail());
    }
}
