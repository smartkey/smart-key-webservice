/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.login;

import com.isk.smartkey.business.login.dto.user.UserCredentialsDto;
import com.isk.smartkey.business.login.dto.user.UserLoginDto;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceDataWrapperDTO;

/**
 *
 * @author Hossam ElDeen
 */
public interface LoginController {
    public ServiceDataWrapperDTO login(UserCredentialsDto userCredentialsDto);
}
