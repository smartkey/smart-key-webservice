/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.login.dto.user;

import com.isk.smartkey.business.common.dto.group.GroupDTO;
import com.isk.smartkey.business.common.dto.permission.PermissionDTO;
import com.isk.smartkey.business.group.dto.user.UserDTO;
import com.isk.smartkey.persistence.entities.User;
import com.isk.smartkey.persistence.entities.UserGroupLink;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author Hossam ElDeen
 */
public class UserLoginDto extends UserDTO {

    private Date lastLogin;
    private Set userHasGroups;

    public UserLoginDto() {
    }

    public UserLoginDto(int id, String fristName, String lastName, String email, String phone, String picture, Set userHasGroups, Date lastLogin) {
        super(id, fristName, lastName, email, phone, picture);
        this.userHasGroups = userHasGroups;
        this.lastLogin = lastLogin;
    }

    public UserLoginDto(User user) {
        this.copyUser(user);
    }

    public Set getUserHasGroups() {
        return userHasGroups;
    }

    public void setUserHasGroups(Set userHasGroups) {
        this.userHasGroups = userHasGroups;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    //FIXME
    public void copyUser(User user) {
        setEmail(user.getEmail());
        setFirstName(user.getFirstName());
        setId(user.getId());
        setLastName(user.getLastName());
        setPhone(user.getPhone());
        setPicture(user.getPicture());
        setLastLogin(user.getLastLogin());

        Set userHasGroupsDTO = new HashSet();
        Iterator iterator = user.getUserGroupLinksList().iterator();
        while (iterator.hasNext()) {
            UserGroupLink ugl = (UserGroupLink) iterator.next();
            Date currentDate = new Date();

            Calendar calendar = Calendar.getInstance();
            int currentDay = calendar.get(Calendar.DAY_OF_WEEK);

            if (currentDate.after(ugl.getStartDate())
                    && (ugl.getEndDate() == null || currentDate.before(ugl.getEndDate()))
                    && ugl.getDays().contains(currentDay + "")
                    && betweenTimes(currentDate, ugl.getStartTime(), ugl.getEndTime())) {

                GroupDTO groupDTO = new GroupDTO(ugl.getGroup());
                PermissionDTO permissionDTO = new PermissionDTO(ugl.getPermission());
                UserHasGroupDTO userHasGroupDTO = new UserHasGroupDTO(groupDTO, permissionDTO);
                userHasGroupsDTO.add(userHasGroupDTO);
            }
        }

        setUserHasGroups(userHasGroupsDTO);
    }

    //FIXME
    private boolean betweenTimes(Date date, Date time1, Date time2) {
        Date d1 = new Date(date.getTime());
        d1.setSeconds(time1.getSeconds());
        d1.setMinutes(time1.getMinutes());
        d1.setHours(time1.getHours());

        if (time2 == null) {
            if (date.after(d1)) {
                return true;
            }else{
                return false;
            }
        }

        Date d2 = new Date(date.getTime());
        d2.setSeconds(time2.getSeconds());
        d2.setMinutes(time2.getMinutes());
        d2.setHours(time2.getHours());

        return (date.after(d1) && date.before(d2));
    }
}
