/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.login.dto.user;

import com.isk.smartkey.business.common.dto.group.GroupDTO;
import com.isk.smartkey.business.common.dto.permission.PermissionDTO;
import com.isk.smartkey.persistence.entities.Group;
import com.isk.smartkey.persistence.entities.Permission;
import com.isk.smartkey.persistence.entities.User;
import java.util.Date;

/**
 *
 * @author Hossam ElDeen
 */
public class UserHasGroupDTO extends GroupDTO{

    private PermissionDTO permissions;

    public UserHasGroupDTO() {
    }

    public UserHasGroupDTO(GroupDTO group, PermissionDTO permissions) {
        super(group.getSecretKey(), group.getName(), group.getSerialNumber());
        this.permissions = permissions;
    }
    
    public PermissionDTO getPermissions() {
        return permissions;
    }
    
    public void setPermissions(PermissionDTO permissions) {
        this.permissions = permissions;
    }

}
