/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.message.impl;

import com.isk.smartkey.business.batteryLog.dto.batteryLog.BatteryLogViewDTO;
import com.isk.smartkey.business.common.BusinessUtil;
import com.isk.smartkey.business.common.LoadingEntitiesController;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceDataWrapperDTO;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceStatusDTO;
import com.isk.smartkey.business.message.MessageController;
import com.isk.smartkey.business.message.dto.Messages.GroupMessageSendingDTO;
import com.isk.smartkey.business.message.dto.Messages.MessageResponseDTO;
import com.isk.smartkey.business.message.dto.Messages.MessageRetrievalDTO;
import com.isk.smartkey.business.message.dto.Messages.UserMessageSendingDTO;
import com.isk.smartkey.business.userLog.dto.userLog.UserLogViewDTO;
import com.isk.smartkey.persistence.dao.batteryLog.BatteryLogDao;
import com.isk.smartkey.persistence.dao.message.MessageDao;
import com.isk.smartkey.persistence.entities.BatteryLog;
import com.isk.smartkey.persistence.entities.Group;
import com.isk.smartkey.persistence.entities.GroupMessage;
import com.isk.smartkey.persistence.entities.User;
import com.isk.smartkey.persistence.entities.UserMessage;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author Hossam ElDeen
 */
@Controller
public class MessageControllerImpl implements MessageController{
    @Autowired(required = true)
    MessageDao messageDao;
    
    @Autowired(required = true)
    LoadingEntitiesController loadingEntitiesController;
    
    @Override
    public ServiceStatusDTO sendMessageToUser(UserMessageSendingDTO userMessageSendingDTO) {
        User fromUser = null;
        User toUser = null;
        
        Object loadUserStatus = loadingEntitiesController.loadUser(userMessageSendingDTO.getFromUserId());
        if (!(loadUserStatus instanceof User)) {
            return (ServiceStatusDTO) loadUserStatus;
        } else {
            fromUser = (User) loadUserStatus;
        }
        
        loadUserStatus = loadingEntitiesController.loadUser(userMessageSendingDTO.getToUserId());
        if (!(loadUserStatus instanceof User)) {
            return (ServiceStatusDTO) loadUserStatus;
        } else {
            toUser = (User) loadUserStatus;
        }

        UserMessage userMessage = new UserMessage(userMessageSendingDTO.getMessageContent(), userMessageSendingDTO.getDate(), toUser, fromUser);
        userMessage.setDeleted(false);

        try {
            messageDao.save(userMessage);
            return new ServiceStatusDTO("SUCCESS", "Message has been sent successfully.");
        } catch (Exception ex) {
            return new ServiceStatusDTO("FAILED", "an internal error has been happened while sending message to user");
        }
    }

    @Override
    public ServiceStatusDTO sendMessageToGroup(GroupMessageSendingDTO groupMessageSendingDTO) {
        User fromUser = null;
        Group toGroup = null;
        
        Object loadUserStatus = loadingEntitiesController.loadUserWithGroupsInitialized(groupMessageSendingDTO.getFromUserId());
        if (!(loadUserStatus instanceof User)) {
            return (ServiceStatusDTO) loadUserStatus;
        } else {
            fromUser = (User) loadUserStatus;
        }
        
        loadUserStatus = loadingEntitiesController.loadGroupWithUserGroupsInitialized(groupMessageSendingDTO.getToGroupId());
        if (!(loadUserStatus instanceof Group)) {
            return (ServiceStatusDTO) loadUserStatus;
        } else {
            toGroup = (Group) loadUserStatus;
        }
        
        ServiceStatusDTO checkUserHasGroupStatus = BusinessUtil.checkUserHasGroup(fromUser, toGroup);
        if (checkUserHasGroupStatus != null) {
            return new ServiceDataWrapperDTO(null, checkUserHasGroupStatus);
        }
        
        GroupMessage groupMessage = new GroupMessage(groupMessageSendingDTO.getMessageContent(), groupMessageSendingDTO.getDate(), toGroup, fromUser);
        groupMessage.setDeleted(false);

        try {
            messageDao.save(groupMessage);
            return new ServiceStatusDTO("SUCCESS", "Message sent successfully.");
        } catch (Exception ex) {
            return new ServiceStatusDTO("FAILED", "an internal error has been happened while sending message to group");
        }
    }

    @Override
    public ServiceDataWrapperDTO getMessagesForGroup(MessageRetrievalDTO messageRetrievalDTO) {
        Group group = null;
        User user = null;
        
        Object loadUserStatus = loadingEntitiesController.loadUserWithGroupsInitialized(messageRetrievalDTO.getFromUserId());
        if(!(loadUserStatus instanceof User)){
            return new ServiceDataWrapperDTO(null, (ServiceStatusDTO)loadUserStatus);
        }else{
            user=(User)loadUserStatus;
        }
        
        loadUserStatus = loadingEntitiesController.loadGroup((String)messageRetrievalDTO.getToObjectId());
        if (!(loadUserStatus instanceof Group)) {
            return new ServiceDataWrapperDTO(null, (ServiceStatusDTO) loadUserStatus);
        } else {
            group = (Group) loadUserStatus;
        }
        
        ServiceStatusDTO checkUserHasGroupStatus = BusinessUtil.checkUserHasGroup(user, group);
        if (checkUserHasGroupStatus != null) {
            return new ServiceDataWrapperDTO(null, checkUserHasGroupStatus);
        }
        
        try {
            List<MessageResponseDTO> messageRetrievalDTOs =  messageDao.getMessagesToGroup(group);
            
            if(messageRetrievalDTOs == null || messageRetrievalDTOs.size()<1){
                return new ServiceDataWrapperDTO(new ArrayList<UserLogViewDTO>(), "SUCCESS", "no messages found.");
            }else{
                return new ServiceDataWrapperDTO(messageRetrievalDTOs, "SUCCESS", "messages for group has been successfully retrieved.");
            }
        } catch (Exception ex) {
            return new ServiceDataWrapperDTO(null,"FAILED", "an internal error has been happened while getting group messages");
        }
    }

    @Override
    public ServiceDataWrapperDTO getMessagesForUser(MessageRetrievalDTO messageRetrievalDTO) {
        User user = null;

        Object loadUserStatus = loadingEntitiesController.loadUser((Integer)messageRetrievalDTO.getToObjectId());
        if (!(loadUserStatus instanceof User)) {
            return new ServiceDataWrapperDTO(null, (ServiceStatusDTO) loadUserStatus);
        } else {
            user = (User) loadUserStatus;
        }

        try {
            List<MessageResponseDTO> messageRetrievalDTOs =  messageDao.getMessagesToUser(user);
            
            if(messageRetrievalDTOs == null || messageRetrievalDTOs.size()<1){
                return new ServiceDataWrapperDTO(new ArrayList<UserLogViewDTO>(), "SUCCESS", "no messages found.");
            }else{
                return new ServiceDataWrapperDTO(messageRetrievalDTOs, "SUCCESS", "private messages for user has been successfully retrieved.");
            }
        } catch (Exception ex) {
            return new ServiceDataWrapperDTO(null,"FAILED", "an internal error has been happened while getting private messages");
        }
    }

    @Override
    public ServiceDataWrapperDTO getAllMessages(MessageRetrievalDTO messageRetrievalDTO) {
        Group group = null;
        User user = null;
        
        Object loadUserStatus = loadingEntitiesController.loadUserWithGroupsInitialized(messageRetrievalDTO.getFromUserId());
        if(!(loadUserStatus instanceof User)){
            return new ServiceDataWrapperDTO(null, (ServiceStatusDTO)loadUserStatus);
        }else{
            user=(User)loadUserStatus;
        }
        
        loadUserStatus = loadingEntitiesController.loadGroup((String)messageRetrievalDTO.getToObjectId());
        if (!(loadUserStatus instanceof Group)) {
            return new ServiceDataWrapperDTO(null, (ServiceStatusDTO) loadUserStatus);
        } else {
            group = (Group) loadUserStatus;
        }
        
        ServiceStatusDTO checkUserHasGroupStatus = BusinessUtil.checkUserHasGroup(user, group);
        if (checkUserHasGroupStatus != null) {
            return new ServiceDataWrapperDTO(null, checkUserHasGroupStatus);
        }
        
        try {
            List<MessageResponseDTO> messageRetrievalDTOs =  messageDao.getAllMessages(group,user,messageRetrievalDTO.getDate());
            
            if(messageRetrievalDTOs == null || messageRetrievalDTOs.size()<1){
                return new ServiceDataWrapperDTO(new ArrayList<UserLogViewDTO>(), "SUCCESS", "no messages found.");
            }else{
                return new ServiceDataWrapperDTO(messageRetrievalDTOs, "SUCCESS", "messages have been successfully retrieved.");
            }
        } catch (Exception ex) {
            Logger.getLogger(MessageControllerImpl.class).error("internal error happened", ex);
            return new ServiceDataWrapperDTO(null,"FAILED", "an internal error has been happened while getting group messages");
        }
    }
    
}
