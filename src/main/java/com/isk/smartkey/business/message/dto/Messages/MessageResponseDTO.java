/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.message.dto.Messages;

import com.isk.smartkey.business.common.serializer.CustomDateSerializer;
import com.isk.smartkey.business.group.dto.user.UserDTO;
import com.isk.smartkey.persistence.entities.GroupMessage;
import com.isk.smartkey.persistence.entities.Message;
import com.isk.smartkey.persistence.entities.User;
import com.isk.smartkey.persistence.entities.UserMessage;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 *
 * @author Hossam ElDeen
 */
public class MessageResponseDTO {
    UserDTO fromUser;
    String messageContent;
    Date date;

    public MessageResponseDTO() {
    }
    
    public MessageResponseDTO(Message message){
        copy(message);
    }
    
    public MessageResponseDTO(User fromUser, String messageContent, Date date) {
        this.fromUser = new UserDTO(fromUser);
        this.messageContent = messageContent;
        this.date = date;
    }

    public UserDTO getFromUserId() {
        return fromUser;
    }

    public void setFromUserId(User fromUser) {
        this.fromUser = new UserDTO(fromUser);
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }
    
    @JsonSerialize(using = CustomDateSerializer.class)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    public void copy(Message message){
        setDate(message.getDate());
        setFromUserId(message.getFromUser());
        setMessageContent(message.getMessageContent());
    }
    
    public static List<MessageResponseDTO> copy(List<? extends Message> messages){
        List<MessageResponseDTO> messageResponseDTOs = new ArrayList<MessageResponseDTO>();
        for(Message message : messages){
            MessageResponseDTO messageResponseDTO = new MessageResponseDTO(message);
            messageResponseDTOs.add(messageResponseDTO);
        }
        return messageResponseDTOs;
    }
}
