/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.message.dto.Messages;

import com.isk.smartkey.business.common.serializer.CustomDateSerializer;
import java.util.Date;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 *
 * @author Hossam ElDeen
 */
public class GroupMessageSendingDTO {
    int fromUserId;
    String toGroupId;
    String messageContent;
    Date date;

    public GroupMessageSendingDTO() {
    }

    public GroupMessageSendingDTO(int fromUserId, String toGroupId, String messageContent, Date date) {
        this.fromUserId = fromUserId;
        this.toGroupId = toGroupId;
        this.messageContent = messageContent;
        this.date = date;
    }

    public int getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(int fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getToGroupId() {
        return toGroupId;
    }

    public void setToGroupId(String toGroupId) {
        this.toGroupId = toGroupId;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    @JsonSerialize(using = CustomDateSerializer.class)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    
}
