/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.message;

import com.isk.smartkey.business.common.dto.businessResponse.ServiceDataWrapperDTO;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceStatusDTO;
import com.isk.smartkey.business.message.dto.Messages.GroupMessageSendingDTO;
import com.isk.smartkey.business.message.dto.Messages.MessageRetrievalDTO;
import com.isk.smartkey.business.message.dto.Messages.UserMessageSendingDTO;

/**
 *
 * @author Hossam ElDeen
 */
public interface MessageController {
    public ServiceStatusDTO sendMessageToUser(UserMessageSendingDTO userMessageSendingDTO);
    public ServiceStatusDTO sendMessageToGroup(GroupMessageSendingDTO groupMessageSendingDTO);
    public ServiceDataWrapperDTO getMessagesForGroup(MessageRetrievalDTO messageRetrievalDTO);
    public ServiceDataWrapperDTO getMessagesForUser(MessageRetrievalDTO messageRetrievalDTO);

    public ServiceDataWrapperDTO getAllMessages(MessageRetrievalDTO messageRetrievalDTO);
}
