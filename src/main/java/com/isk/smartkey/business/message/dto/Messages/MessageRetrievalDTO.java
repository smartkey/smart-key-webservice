/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.message.dto.Messages;

import com.isk.smartkey.business.common.serializer.CustomDateSerializer;
import java.util.Date;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 *
 * @author Hossam ElDeen
 */
public class MessageRetrievalDTO {
    Object toObjectId;
    Integer fromUserId;
    Date date;
    
    public MessageRetrievalDTO() {
    }

    public MessageRetrievalDTO(Object toObjectId, Integer fromUserId) {
        this.toObjectId = toObjectId;
        this.fromUserId = fromUserId;
    }

    public MessageRetrievalDTO(Object toObjectId, Integer fromUserId, Date date) {
        this.toObjectId = toObjectId;
        this.fromUserId = fromUserId;
        this.date = date;
    }

    @JsonSerialize(using = CustomDateSerializer.class)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

   
    

    public int getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(int fromUserId) {
        this.fromUserId = fromUserId;
    }
    
    
    public Object getToObjectId() {
        return toObjectId;
    }

    public void setToObjectId(Object toObjectId) {
        this.toObjectId = toObjectId;
    }
    
}
