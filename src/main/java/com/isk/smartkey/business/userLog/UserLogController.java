/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.userLog;

import com.isk.smartkey.business.common.dto.businessResponse.ServiceDataWrapperDTO;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceStatusDTO;
import com.isk.smartkey.business.userLog.dto.userLog.UserLogRetrievalDTO;
import com.isk.smartkey.business.userLog.dto.userLog.UserLogUpdateDTO;

/**
 *
 * @author Hossam ElDeen
 */
public interface UserLogController {
    public ServiceStatusDTO update(UserLogUpdateDTO userLogUpdateDTO);
    public ServiceDataWrapperDTO get(UserLogRetrievalDTO userLogRetrievalDTO);
}
