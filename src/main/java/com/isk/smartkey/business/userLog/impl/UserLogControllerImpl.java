/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.userLog.impl;

import com.isk.smartkey.business.common.BusinessUtil;
import com.isk.smartkey.business.common.LoadingEntitiesController;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceDataWrapperDTO;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceStatusDTO;
import com.isk.smartkey.business.userLog.UserLogController;
import com.isk.smartkey.business.userLog.dto.userLog.UserLogRetrievalDTO;
import com.isk.smartkey.business.userLog.dto.userLog.UserLogUpdateDTO;
import com.isk.smartkey.business.userLog.dto.userLog.UserLogViewDTO;
import com.isk.smartkey.persistence.dao.userLog.UserLogDao;
import com.isk.smartkey.persistence.entities.Group;
import com.isk.smartkey.persistence.entities.User;
import com.isk.smartkey.persistence.entities.UserLog;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author Hossam ElDeen
 */
@Controller
public class UserLogControllerImpl implements UserLogController {

    @Autowired(required = true)
    UserLogDao userLogDao;

    @Autowired(required = true)
    LoadingEntitiesController loadingEntitiesController;

    @Override
    public ServiceStatusDTO update(UserLogUpdateDTO userLogUpdateDTO) {
        User user = null;
        Group group = null;

        Object loadUserStatus = loadingEntitiesController.loadUser(userLogUpdateDTO.getUserId());
        if (!(loadUserStatus instanceof User)) {
            return (ServiceStatusDTO) loadUserStatus;
        } else {
            user = (User) loadUserStatus;
        }

        loadUserStatus = loadingEntitiesController.loadGroup(userLogUpdateDTO.getGroupId());
        if (!(loadUserStatus instanceof Group)) {
            return (ServiceStatusDTO) loadUserStatus;
        } else {
            group = (Group) loadUserStatus;
        }

        UserLog userLog = new UserLog(userLogUpdateDTO.getDate(), group, user);
        userLog.setDeleted(false);

        try {
            userLogDao.save(userLog);
            return new ServiceStatusDTO("SUCCESS", "Log has been updated successfully.");
        } catch (Exception ex) {
            return new ServiceStatusDTO("FAILED", "an internal error has been happened while creating group.");
        }
    }

    @Override
    public ServiceDataWrapperDTO get(UserLogRetrievalDTO userLogRetrievalDTO) {
        Group group = null;
        User user = null;
        
        Object loadUserStatus = loadingEntitiesController.loadUserWithGroupsInitialized(userLogRetrievalDTO.getUserId());
        if(!(loadUserStatus instanceof User)){
            return new ServiceDataWrapperDTO(null, (ServiceStatusDTO) loadUserStatus);
        }else{
            user = (User) loadUserStatus;
        }
        
        loadUserStatus = loadingEntitiesController.loadGroup(userLogRetrievalDTO.getGroupId());
        if (!(loadUserStatus instanceof Group)) {
            return new ServiceDataWrapperDTO(null, (ServiceStatusDTO) loadUserStatus);
        } else {
            group = (Group) loadUserStatus;
        }
        
        
        ServiceStatusDTO checkUserHasGroup = BusinessUtil.checkUserHasGroup(user, group);
        if(checkUserHasGroup != null){
            return new ServiceDataWrapperDTO(null, checkUserHasGroup);
        }

        try {
            List<UserLogViewDTO> userLogs =  userLogDao.getLogsOfGroup(group);
            if(userLogs == null || userLogs.size()<1){
                return new ServiceDataWrapperDTO(new ArrayList<UserLogViewDTO>(), "SUCCESS", "no user logs found.");
            }else{
                return new ServiceDataWrapperDTO(userLogs, "SUCCESS", "user logs have been retrieved successfully.");
            }
        } catch (Exception ex) {
            return new ServiceDataWrapperDTO(null,"FAILED", "an internal error has been happened while creating group.");
        }
    }

}
