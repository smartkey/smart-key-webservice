/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.userLog.dto.userLog;

import com.isk.smartkey.business.common.serializer.CustomDateSerializer;
import com.isk.smartkey.business.group.dto.user.UserDTO;
import com.isk.smartkey.persistence.entities.UserLog;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 *
 * @author Hossam ElDeen
 */
public class UserLogViewDTO {
    UserDTO userOfGroupDTO;
    Date date;

    public UserLogViewDTO() {
    }
    
    public UserLogViewDTO(UserLog userLog){
        this.copy(userLog);
    }

    public UserLogViewDTO(UserDTO userOfGroupDTO, Date date) {
        this.userOfGroupDTO = userOfGroupDTO;
        this.date = date;
    }

    public UserDTO getUserOfGroupDTO() {
        return userOfGroupDTO;
    }

    public void setUserOfGroupDTO(UserDTO userOfGroupDTO) {
        this.userOfGroupDTO = userOfGroupDTO;
    }

    @JsonSerialize(using = CustomDateSerializer.class)
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
    public final void copy(UserLog userLog){
        setDate(userLog.getDate());
        UserDTO userOfGroupDTO = new UserDTO(userLog.getUser());
        setUserOfGroupDTO(userOfGroupDTO);
    }
    
    public static List<UserLogViewDTO> copy(List<UserLog> userLogs){
        if(userLogs == null)
            return null;
        if(userLogs.size() < 1)
            return new ArrayList<UserLogViewDTO>();
        
        List<UserLogViewDTO> userLogViewDTOs = new ArrayList<UserLogViewDTO>();
        for(UserLog userLog: userLogs){
            UserLogViewDTO userLogViewDTO = new UserLogViewDTO(userLog);
            userLogViewDTOs.add(userLogViewDTO);
        }
        return userLogViewDTOs;
    }
}
