/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.userLog.dto.userLog;

/**
 *
 * @author Hossam ElDeen
 */
public class UserLogRetrievalDTO {
    int userId;
    String groupId;

    public UserLogRetrievalDTO() {
    }

    public UserLogRetrievalDTO(int userId, String groupId) {
        this.userId = userId;
        this.groupId = groupId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

}
