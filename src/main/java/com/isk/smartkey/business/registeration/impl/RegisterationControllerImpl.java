/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.registeration.impl;

import com.isk.smartkey.business.common.dto.businessResponse.ServiceStatusDTO;
import com.isk.smartkey.business.common.util.DataBaseUtil;
import com.isk.smartkey.business.common.constants.RegexConstants;
import com.isk.smartkey.business.registeration.RegisterationController;
import com.isk.smartkey.business.registeration.dto.user.UserRegisterDTO;
import com.isk.smartkey.business.registeration.util.PasswordHash;
import com.isk.smartkey.persistence.dao.user.UserDao;
import com.isk.smartkey.persistence.dao.user.impl.UserDaoImpl;
import com.isk.smartkey.persistence.entities.User;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 *
 * @author Hossam ElDeen
 */
@Controller
public class RegisterationControllerImpl implements RegisterationController {

    @Autowired(required = true)
    UserDao userDao;

    @Override
    public ServiceStatusDTO registerUser(UserRegisterDTO userRegisterDTO) {
        if (userRegisterDTO == null) {
            throw new IllegalArgumentException("UserRegisterDTO can not be null");
        }

        User user = new User("values to achieve happy scenario3", userRegisterDTO.getFirstName(), userRegisterDTO.getLastName(), userRegisterDTO.getEmail(), userRegisterDTO.getPhone(), null, null, userRegisterDTO.getCreatedAt(), userRegisterDTO.getUpdatedAt(), userRegisterDTO.getPicture());
        user.setDeleted(false);

        ServiceStatusDTO serviceStatusDTO;
        try {

            String errorMessage = validateData(user);
            if (errorMessage.isEmpty()) {
                String[] hashedData = PasswordHash.createHash(userRegisterDTO.getPassword());
                user.setEncryptedPassword(hashedData[PasswordHash.PBKDF2_INDEX - 1]);
                user.setSalt(hashedData[PasswordHash.SALT_INDEX - 1]);
                
                BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(11);
                String password = encoder.encode(userRegisterDTO.getPassword());
                String NativeQuery = "INSERT INTO oauth_client_details(client_id, resource_ids, client_secret, scope, authorized_grant_types, authorities, access_token_validity, refresh_token_validity) VALUES ('" + userRegisterDTO.getEmail() + "', 'rest_api', '" + password + "', 'trust,read,write', 'client_credentials,authorization_code,implicit,password,refresh_token', 'ROLE_USER', '4500', '45000');";

                
                userDao.createUserCred(NativeQuery);
                userDao.save(user);

                
                serviceStatusDTO = new ServiceStatusDTO("SUCCESS", "user has been registered successfully.");
            } else {
                serviceStatusDTO = new ServiceStatusDTO("FAILED", errorMessage);
            }
        } catch (Exception ex) {
            Logger.getLogger(RegisterationControllerImpl.class.getName()).log(Level.SEVERE, null, ex);
            serviceStatusDTO = new ServiceStatusDTO("FAILED", "an internal error has been happened, please try again later.");
        }
        return serviceStatusDTO;
    }

    private String validateData(User user) {
        String formatErrorMessage = validateFormatOfData(user);
        if (formatErrorMessage.isEmpty()) {
            String databaseErrorMessage = validatePersistenceOfData(user);
            return databaseErrorMessage;
        } else {
            return formatErrorMessage;
        }

    }

    private String validateFormatOfData(User user) {
        int errorFlag = 0;
        String formatErrorMessage = "";
        if (!user.getFirstName().matches(RegexConstants.NAME_REGEX)) {
            formatErrorMessage += "first name is not in a proper format";
            errorFlag++;
        }
        if (!user.getLastName().matches(RegexConstants.NAME_REGEX)) {
            if (errorFlag > 0) {
                formatErrorMessage += " and ";
            } else {
                errorFlag++;
            }
            formatErrorMessage += "last name is not in a proper format";
        }
        if (!user.getPhone().matches(RegexConstants.PHONE_REGEX)) {
            if (errorFlag > 0) {
                formatErrorMessage += " and ";
            } else {
                errorFlag++;
            }
            formatErrorMessage += "phone is not in a proper format";
        }
        if (!user.getEmail().matches(RegexConstants.EMAIL_REGEX)) {
            if (errorFlag > 0) {
                formatErrorMessage += " and ";
            }
            formatErrorMessage += "email is not in a proper format";
        }
        return formatErrorMessage;
    }

    private String validatePersistenceOfData(User user) {
        int errorFlag = 0;
        String errorMessage = "";

        if (userDao.isUserExist(user.getEmail())) {
            errorMessage += "user with the same email already exist";
            errorFlag++;
        }
        if (userDao.isUserExistWithTheSamePhone(user.getPhone())) {
            if (errorFlag > 0) {
                errorMessage += " and ";
            }
            errorMessage += "user with the same phone already exist";
        }
        return errorMessage;
    }
}
