/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.registeration;

import com.isk.smartkey.business.common.dto.businessResponse.ServiceStatusDTO;
import com.isk.smartkey.business.registeration.dto.user.UserRegisterDTO;

/**
 *
 * @author Hossam ElDeen
 */
public interface RegisterationController {
    public ServiceStatusDTO registerUser(UserRegisterDTO userRegisterDTO);
}
