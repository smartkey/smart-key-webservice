/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.registeration.dto.user;

import com.isk.smartkey.business.common.serializer.CustomDateSerializer;
import java.util.Date;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 *
 * @author Hossam ElDeen
 */
public class UserRegisterDTO {

    private String uniqueId;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String password;
    private Date createdAt;
    private Date updatedAt;
    private String picture;

    public UserRegisterDTO() {
        init();
    }

    public UserRegisterDTO(String firstName, String lastName, String email, String phone, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.password = password;
        init();
    }

    private void init() {
        createdAt = new Date();
        updatedAt = createdAt;
        picture = "/resources/images/defaultProfilePic.jpg";
        //salt
        //unique id
    }

    public String getUniqueId() {
        return uniqueId;
    }


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getPassword() {
        return password;
    }

    @JsonSerialize(using = CustomDateSerializer.class)
    public Date getCreatedAt() {
        return createdAt;
    }

    @JsonSerialize(using = CustomDateSerializer.class)
    public Date getUpdatedAt() {
        return updatedAt;
    }

    public String getPicture() {
        return picture;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
