/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.group.dto.group;

/**
 *
 * @author Hossam ElDeen
 */
public class GetUserOnGroupDTO {

    String groupId;
    int userId;

    public GetUserOnGroupDTO() {
    }

    public GetUserOnGroupDTO(String groupId, int userId) {
        this.groupId = groupId;
        this.userId = userId;
    }

    public String getGroupId() {
        return groupId;
    }

    public int getUserId() {
        return userId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

}
