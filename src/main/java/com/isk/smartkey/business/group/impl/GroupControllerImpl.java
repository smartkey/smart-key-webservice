/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.group.impl;

import com.isk.smartkey.business.common.BusinessUtil;
import com.isk.smartkey.business.common.LoadingEntitiesController;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceDataWrapperDTO;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceStatusDTO;
import com.isk.smartkey.business.group.GroupController;
import com.isk.smartkey.business.group.dto.group.AddUserToGroupDTO;
import com.isk.smartkey.business.group.dto.group.DeleteUserFromGroupDTO;
import com.isk.smartkey.business.group.dto.group.GetUserOnGroupDTO;
import com.isk.smartkey.business.common.dto.group.GroupDTO;
import com.isk.smartkey.business.group.dto.user.UserDTO;
import com.isk.smartkey.business.registeration.util.PasswordHash;
import com.isk.smartkey.persistence.dao.group.GroupDao;
import com.isk.smartkey.persistence.dao.permission.PermissionDao;
import com.isk.smartkey.persistence.dao.permission.constants.PermissionConstants;
import com.isk.smartkey.persistence.dao.user.UserDao;
import com.isk.smartkey.persistence.entities.Group;
import com.isk.smartkey.persistence.entities.Permission;
import com.isk.smartkey.persistence.entities.User;
import com.isk.smartkey.persistence.entities.UserGroupLink;
import com.isk.smartkey.persistence.entities.UserGroupLinkPK;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;

/**
 *
 * @author Hossam ElDeen
 */
@Controller
public class GroupControllerImpl implements GroupController {

    @Autowired(required = true)
    GroupDao groupDao;

    @Autowired(required = true)
    UserDao userDao;

    @Autowired(required = true)
    PermissionDao PermissionDao;

    @Autowired(required = true)
    LoadingEntitiesController loadingEntitiesController;

    @Override
    public ServiceStatusDTO createGroup(GroupDTO groupCreationDTO, int userId) {
        Group group = new Group();
        Permission fullPermission = null;
        User user = null;

        Object loadUserStatus = loadingEntitiesController.loadUserWithGroupsInitialized(userId);
        if (!(loadUserStatus instanceof User)) {
            return (ServiceStatusDTO) loadUserStatus;
        } else {
            user = (User) loadUserStatus;
        }

        Object loadPermissionStatus = loadingEntitiesController.loadPermissionWithUserGroupInitialized(PermissionConstants.FULL_PERMISSION);
        if (!(loadPermissionStatus instanceof Permission)) {
            return (ServiceStatusDTO) loadPermissionStatus;
        } else {
            fullPermission = (Permission) loadPermissionStatus;
        }

        Object loadGroupStatus = loadingEntitiesController.loadGroupWithUserGroupsInitialized(groupCreationDTO.getSerialNumber());
        if (!(loadGroupStatus instanceof Group)) {
            return (ServiceStatusDTO) loadGroupStatus;
        } else {
            group = (Group) loadGroupStatus;
        }

        try {
            group.setDeleted(false);
            String[] hashedData = PasswordHash.createHash(groupCreationDTO.getSecretKey());
            group.setSecretKey(hashedData[PasswordHash.PBKDF2_INDEX - 1]);
            group.setSalt(hashedData[PasswordHash.SALT_INDEX - 1]);
            group.setName(groupCreationDTO.getName());
            groupDao.update(group);
            UserGroupLink userGroupLink = addUserToGroup(group, user, fullPermission, null);

            groupDao.save(userGroupLink);
            return new ServiceStatusDTO("SUCCESS", "group created successfully.");
        } catch (Exception ex) {
            Logger.getLogger(GroupControllerImpl.class).error("an error happened", ex);
            return new ServiceStatusDTO("FAILED", "an internal error has been happened while creating group.");
        }
    }

    @Override
    public ServiceStatusDTO addUserToGroup(AddUserToGroupDTO addUserToGroupDTO) {
        User user = null;
        User userToBeAdded = null;
        Group group = null;
        Permission permission = null;
        Permission permissionToBeAdded = null;

        Object loadUserStatus = loadingEntitiesController.loadUser(addUserToGroupDTO.getUserId());
        if (!(loadUserStatus instanceof User)) {
            return (ServiceStatusDTO) loadUserStatus;
        } else {
            user = (User) loadUserStatus;
        }

        ServiceStatusDTO verifyUserStatus = verifyUser(user, addUserToGroupDTO.getPassword());
        if (verifyUserStatus != null) {
            return verifyUserStatus;
        }

        Object loadGroupStatus = loadingEntitiesController.loadGroupWithUserGroupsInitialized(addUserToGroupDTO.getGroupId());
        if (!(loadGroupStatus instanceof Group)) {
            return (ServiceStatusDTO) loadGroupStatus;
        } else {
            group = (Group) loadGroupStatus;
        }

        Object checkPermissionOfManageUsersOnGroupStatus = checkPermissionOfManageUsersOnGroup(group, user);
        if (!(checkPermissionOfManageUsersOnGroupStatus instanceof Permission)) {
            return (ServiceStatusDTO) checkPermissionOfManageUsersOnGroupStatus;
        } else {
            permission = (Permission) checkPermissionOfManageUsersOnGroupStatus;
        }

        if (addUserToGroupDTO.getUserToBeAddedEmail() == null && addUserToGroupDTO.getUserToBeAddedPhone() == null) {
            loadUserStatus = loadingEntitiesController.loadUserWithGroupsInitialized(addUserToGroupDTO.getUserToBeAddedId());
            if (!(loadUserStatus instanceof User)) {
                return (ServiceStatusDTO) loadUserStatus;
            } else {
                userToBeAdded = (User) loadUserStatus;
            }
        } else if (addUserToGroupDTO.getUserToBeAddedPhone() == null) {
            loadUserStatus = userDao.loadUserWithGroupsInitializedViaEmail(addUserToGroupDTO.getUserToBeAddedEmail());
            if (!(loadUserStatus instanceof User)) {
                return (ServiceStatusDTO) loadUserStatus;
            } else {
                userToBeAdded = (User) loadUserStatus;
            }
        } else if (addUserToGroupDTO.getUserToBeAddedEmail() == null) {
            loadUserStatus = userDao.loadUserWithGroupsInitializedViaPhone(addUserToGroupDTO.getUserToBeAddedPhone());
            if (!(loadUserStatus instanceof User)) {
                return (ServiceStatusDTO) loadUserStatus;
            } else {
                userToBeAdded = (User) loadUserStatus;
            }
        }

        Object loadPermissionStatus = loadingEntitiesController.loadPermissionWithUserGroupInitialized(addUserToGroupDTO.getPermissionId());
        if (!(loadPermissionStatus instanceof Permission)) {
            return (ServiceStatusDTO) loadPermissionStatus;
        } else {
            permissionToBeAdded = (Permission) loadPermissionStatus;
        }

        ServiceDataWrapperDTO serviceDataWrapperDTO = checkUserExistOnGroup(userToBeAdded, group);
        if (serviceDataWrapperDTO.getData() == null) {
            return (ServiceStatusDTO) serviceDataWrapperDTO;
        } else if ((Boolean) serviceDataWrapperDTO.getData() == true) {
            ServiceStatusDTO serviceStatusDTO = (ServiceStatusDTO) serviceDataWrapperDTO;
            serviceStatusDTO.setStatus("FAILED");
            return serviceStatusDTO;
        }

        UserGroupLink userGroupLink = addUserToGroup(group, userToBeAdded, permissionToBeAdded, addUserToGroupDTO);

        try {
            userDao.save(userGroupLink);
            userDao.update(user);
            return new ServiceStatusDTO("SUCCESS", "user has been added to group successfully.");
        } catch (Exception ex) {
            return new ServiceStatusDTO("FAILED", "an internal error has been happened.");
        }
    }

    @Override
    public ServiceStatusDTO deleteUserFromGroup(DeleteUserFromGroupDTO deleteUserFromGroupDTO) {
        User user = null;
        User userToBeDeleted = null;
        Group group = null;
        Permission permission = null;
        UserGroupLink userGroupLink = null;

        Object loadUserStatus = loadingEntitiesController.loadUser(deleteUserFromGroupDTO.getUserId());
        if (!(loadUserStatus instanceof User)) {
            return (ServiceStatusDTO) loadUserStatus;
        } else {
            user = (User) loadUserStatus;
        }

        ServiceStatusDTO verifyUserStatus = verifyUser(user, deleteUserFromGroupDTO.getPassword());
        if (verifyUserStatus != null) {
            return verifyUserStatus;
        }

        Object loadGroupStatus = loadingEntitiesController.loadGroupWithUserGroupsInitialized(deleteUserFromGroupDTO.getGroupId());
        if (!(loadGroupStatus instanceof Group)) {
            return (ServiceStatusDTO) loadGroupStatus;
        } else {
            group = (Group) loadGroupStatus;
        }

        Object checkPermissionOfManageUsersOnGroupStatus = checkPermissionOfManageUsersOnGroup(group, user);
        if (!(checkPermissionOfManageUsersOnGroupStatus instanceof Permission)) {
            return (ServiceStatusDTO) checkPermissionOfManageUsersOnGroupStatus;
        } else {
            permission = (Permission) checkPermissionOfManageUsersOnGroupStatus;
        }

        loadUserStatus = loadingEntitiesController.loadUserWithGroupsInitialized(deleteUserFromGroupDTO.getUserToBeDeletedId());
        if (!(loadUserStatus instanceof User)) {
            return (ServiceStatusDTO) loadUserStatus;
        } else {
            userToBeDeleted = (User) loadUserStatus;
        }

        Object loadUserHasGroupStatus = loadUserHasGroup(group, userToBeDeleted);
        if (!(loadUserHasGroupStatus instanceof UserGroupLink)) {
            return (ServiceStatusDTO) loadUserHasGroupStatus;
        } else {
            userGroupLink = (UserGroupLink) loadUserHasGroupStatus;
        }

        removeUserFromGroup(userGroupLink, group, userToBeDeleted, permission);

        try {
            userDao.delete(userGroupLink);
            userDao.update(user);
            return new ServiceStatusDTO("SUCCESS", "user has been deleted from group successfully.");
        } catch (Exception ex) {
            return new ServiceStatusDTO("FAILED", "an internal error has been happened.");
        }
    }

    @Override
    public ServiceDataWrapperDTO getUsersOnGroup(GetUserOnGroupDTO userOnGroupDTO) {
        User user = null;
        Group group = null;

        Object loadUserStatus = loadingEntitiesController.loadUserWithGroupsInitialized(userOnGroupDTO.getUserId());
        if (!(loadUserStatus instanceof User)) {
            return new ServiceDataWrapperDTO(null, (ServiceStatusDTO) loadUserStatus);
        } else {
            user = (User) loadUserStatus;
        }

        Object loadGroupStatus = loadingEntitiesController.loadGroup(userOnGroupDTO.getGroupId());
        if (!(loadGroupStatus instanceof Group)) {
            return new ServiceDataWrapperDTO(null, (ServiceStatusDTO) loadGroupStatus);
        } else {
            group = (Group) loadGroupStatus;
        }

        ServiceStatusDTO checkUserHasGroupStatus = BusinessUtil.checkUserHasGroup(user, group);
        if (checkUserHasGroupStatus != null) {
            return new ServiceDataWrapperDTO(null, checkUserHasGroupStatus);
        }

        try {
            List<UserDTO> users = userDao.getUsersOnGroup(group);
            //I did not check for null as this criteria should at least returns the current user
            //this is because to get users on group you have to be member of the group
            ServiceStatusDTO serviceStatusDTO = new ServiceStatusDTO("SUCCESS", "users on group have been successfully retrieved.");
            return new ServiceDataWrapperDTO(users, serviceStatusDTO);
        } catch (Exception ex) {
            ServiceStatusDTO serviceStatusDTO = new ServiceStatusDTO("FAILED", "an internal error has been happened.");
            return new ServiceDataWrapperDTO(null, serviceStatusDTO);
        }
    }

    private Object loadUserHasGroup(Group group, User user) {
        UserGroupLink userGroupLink;
        try {
            userGroupLink = groupDao.getUserHasGroup(group, user);
        } catch (Exception ex) {
            return new ServiceStatusDTO("FAILED", "an internal error has been happened.");
        }

        if (userGroupLink == null) {
            return new ServiceStatusDTO("FAILED", "user does not belong to that group.");
        } else {
            return userGroupLink;
        }
    }

    private UserGroupLink addUserToGroup(Group group, User user, Permission permission, AddUserToGroupDTO autg) {
        UserGroupLinkPK userGroupLinkPK = new UserGroupLinkPK(user.getId(), group.getId());
        UserGroupLink userGroupLink;
        if (autg == null) {
            userGroupLink = new UserGroupLink(userGroupLinkPK, group, permission, user);
            userGroupLink.setStartDate(new Date());
            userGroupLink.setStartTime(new Date());
            userGroupLink.setDays("1234567");
            userGroupLink.setDeleted(false);
        } else {
            if(autg.getStartDate() == null)
                autg.setStartDate(new Date());
            if(autg.getStartTime() == null)
                autg.setStartTime(new Date());
            userGroupLink = new UserGroupLink(userGroupLinkPK, autg.getStartDate(), autg.getEndDate(), autg.getStartTime(), autg.getEndTime(), autg.getDays(), false, group, permission, user);
        }

        if (group.getUserGroupLinksList() != null) {
            group.getUserGroupLinksList().add(userGroupLink);
        } else {
            List<UserGroupLink> userGroupLinks = new ArrayList<UserGroupLink>();
            userGroupLinks.add(userGroupLink);
            group.setUserGroupLinksList(userGroupLinks);
        }

        if (user.getUserGroupLinksList() != null) {
            user.getUserGroupLinksList().add(userGroupLink);
        } else {
            List<UserGroupLink> userGroupLinks = new ArrayList<UserGroupLink>();
            userGroupLinks.add(userGroupLink);
            user.setUserGroupLinksList(userGroupLinks);
        }

        if (permission.getUserGroupLinksList() != null) {
            permission.getUserGroupLinksList().add(userGroupLink);
        } else {
            List<UserGroupLink> userGroupLinks = new ArrayList<UserGroupLink>();
            userGroupLinks.add(userGroupLink);
            permission.setUserGroupLinksList(userGroupLinks);
        }

        return userGroupLink;
    }

    private void removeUserFromGroup(UserGroupLink userGroupLink, Group group, User user, Permission permission) {
        group.getUserGroupLinksList().remove(userGroupLink);
        user.getUserGroupLinksList().remove(userGroupLink);
        permission.getUserGroupLinksList().remove(userGroupLink);
    }

    private ServiceStatusDTO verifyUser(User user, String password) {
        try {
            if (PasswordHash.validatePassword(password, user.getEncryptedPassword())) {
                return null;
            } else {
                return new ServiceStatusDTO("FAILED", "user credentials are not correct.");
            }
        } catch (NoSuchAlgorithmException ex) {
            java.util.logging.Logger.getLogger(GroupControllerImpl.class.getName()).log(Level.SEVERE, null, ex);
            return new ServiceStatusDTO("FAILED", "error has been happened.");
        } catch (InvalidKeySpecException ex) {
            java.util.logging.Logger.getLogger(GroupControllerImpl.class.getName()).log(Level.SEVERE, null, ex);
            return new ServiceStatusDTO("FAILED", "error has been happened.");
        }
    }

    private Object checkPermissionOfManageUsersOnGroup(Group group, User user) {
        Permission permission = PermissionDao.getPermissionOfUserOnGroup(group, user);
        if (permission == null) {
            return new ServiceStatusDTO("FAILED", "user does not belong to this group.");
        } else if (!permission.isManageUsers()) {
            return new ServiceStatusDTO("FAILED", "user does not have permission to add other users.");
        } else {
            return permission;
        }
    }

    private ServiceDataWrapperDTO checkUserExistOnGroup(User userToBeAdded, Group group) {
        UserGroupLink userGroupLink;
        try {
            userGroupLink = userDao.loadUserGroupLink(userToBeAdded, group);
        } catch (Exception ex) {
            return new ServiceDataWrapperDTO(null, "FAILED", "an internal error has been happened.");
        }

        if (userGroupLink == null) {
            return new ServiceDataWrapperDTO(false, "SUCCESS", "user does not belong to that group");
        } else {
            return new ServiceDataWrapperDTO(true, "SUCCESS", "user belongs to that group");
        }
    }

}
