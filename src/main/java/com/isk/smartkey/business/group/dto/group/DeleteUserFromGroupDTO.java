/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.group.dto.group;

import java.util.Date;

/**
 *
 * @author Hossam ElDeen
 */
public class DeleteUserFromGroupDTO {

    private String groupId;
    private int userId;
    private String password;
    private int userToBeDeletedId;

    public DeleteUserFromGroupDTO() {
    }

    public DeleteUserFromGroupDTO(String groupId, int userId, String password, int userToBeAddedId) {
        this.groupId = groupId;
        this.userId = userId;
        this.password = password;
        this.userToBeDeletedId = userToBeAddedId;
    }

    public String getGroupId() {
        return groupId;
    }

    public int getUserId() {
        return userId;
    }

    public String getPassword() {
        return password;
    }

    public int getUserToBeDeletedId() {
        return userToBeDeletedId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserToBeDeletedId(int userToBeAddedId) {
        this.userToBeDeletedId = userToBeAddedId;
    }
}
