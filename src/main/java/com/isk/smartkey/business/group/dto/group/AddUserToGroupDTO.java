/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.group.dto.group;

import com.isk.smartkey.business.common.serializer.CustomDateSerializer;
import java.util.Date;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 *
 * @author Hossam ElDeen
 */
public class AddUserToGroupDTO {

    private String groupId;
    private int userId;
    private String password;
    
    private int userToBeAddedId;
    private String userToBeAddedEmail;
    private String userToBeAddedPhone;
    
    private Date startDate;
    private Date endDate;
    private Date startTime;
    private Date endTime;
    private String days;
    
    private int permissionId;

    public AddUserToGroupDTO() {
    }

    public AddUserToGroupDTO(String groupId, int userId, String password, int userToBeAddedId, 
            Date startDate, Date endDate, Date startTime, Date endTime, String days, int permissionId) {
        this.groupId = groupId;
        this.userId = userId;
        this.password = password;
        this.userToBeAddedId = userToBeAddedId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.days = days;
        this.permissionId = permissionId;
    }

    public AddUserToGroupDTO(String groupId, int userId, String password, String userToBeAddedEmail, Date startDate, Date endDate, Date startTime, Date endTime, String days, int permissionId) {
        this.groupId = groupId;
        this.userId = userId;
        this.password = password;
        this.userToBeAddedEmail = userToBeAddedEmail;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.days = days;
        this.permissionId = permissionId;
    }
    
    public AddUserToGroupDTO(String groupId, int userId, String password, Date startDate, Date endDate, Date startTime, Date endTime, String days, int permissionId, String userToBeAddedPhone) {
        this.groupId = groupId;
        this.userId = userId;
        this.password = password;
        this.userToBeAddedPhone = userToBeAddedPhone;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.days = days;
        this.permissionId = permissionId;
    }

    public String getUserToBeAddedPhone() {
        return userToBeAddedPhone;
    }

    public void setUserToBeAddedPhone(String userToBeAddedPhone) {
        this.userToBeAddedPhone = userToBeAddedPhone;
    }
    
    public String getUserToBeAddedEmail() {
        return userToBeAddedEmail;
    }

    public void setUserToBeAddedEmail(String userToBeAddedEmail) {
        this.userToBeAddedEmail = userToBeAddedEmail;
    }
    
    
    public void setPermissionId(int permissionId) {
        this.permissionId = permissionId;
    }

    public int getPermissionId() {
        return permissionId;
    }

    public String getDays() {
        return days;
    }

    @JsonSerialize(using = CustomDateSerializer.class)
    public Date getEndDate() {
        return endDate;
    }

    @JsonSerialize(using = CustomDateSerializer.class)
    public Date getEndTime() {
        return endTime;
    }

    @JsonSerialize(using = CustomDateSerializer.class)
    public Date getStartDate() {
        return startDate;
    }

    @JsonSerialize(using = CustomDateSerializer.class)
    public Date getStartTime() {
        return startTime;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public String getGroupId() {
        return groupId;
    }

    public int getUserId() {
        return userId;
    }

    public String getPassword() {
        return password;
    }

    public int getUserToBeAddedId() {
        return userToBeAddedId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserToBeAddedId(int userToBeAddedId) {
        this.userToBeAddedId = userToBeAddedId;
    }
}
