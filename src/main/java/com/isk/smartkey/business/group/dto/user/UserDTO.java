/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.group.dto.user;

import com.isk.smartkey.business.common.dto.group.GroupDTO;
import com.isk.smartkey.business.common.dto.permission.PermissionDTO;
import com.isk.smartkey.business.login.dto.user.UserHasGroupDTO;
import com.isk.smartkey.persistence.entities.User;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Hossam ElDeen
 */
public class UserDTO {

    private int id;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String picture;

    public UserDTO() {
    }

    public UserDTO(int id, String fristName, String lastName, String email, String phone, String picture) {
        this.id = id;
        this.firstName = fristName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.picture = picture;
    }

    public UserDTO(User user) {
        this.copyUser(user);
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getPicture() {
        return picture;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String fristName) {
        this.firstName = fristName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public void copyUser(User user) {
        setEmail(user.getEmail());
        setFirstName(user.getFirstName());
        setId(user.getId());
        setLastName(user.getLastName());
        setPhone(user.getPhone());
        setPicture(user.getPicture());
    }

    public static List copyList(List<User> users) {
        List<UserDTO> userOfGroupDTOs = new ArrayList<UserDTO>();
        for (User user : users) {
            userOfGroupDTOs.add(new UserDTO(user));
        }
        return userOfGroupDTOs;
    }
}
