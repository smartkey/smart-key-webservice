/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.group;

import com.isk.smartkey.business.common.dto.businessResponse.ServiceDataWrapperDTO;
import com.isk.smartkey.business.common.dto.businessResponse.ServiceStatusDTO;
import com.isk.smartkey.business.group.dto.group.AddUserToGroupDTO;
import com.isk.smartkey.business.group.dto.group.DeleteUserFromGroupDTO;
import com.isk.smartkey.business.group.dto.group.GetUserOnGroupDTO;
import com.isk.smartkey.business.common.dto.group.GroupDTO;

/**
 *
 * @author Hossam ElDeen
 */
public interface GroupController {
    public ServiceStatusDTO createGroup(GroupDTO groupCreationDTO,int userId);
    
    public ServiceStatusDTO addUserToGroup(AddUserToGroupDTO addUserToGroupDTO);

    public ServiceStatusDTO deleteUserFromGroup(DeleteUserFromGroupDTO deleteUserFromGroupDTO);

    public ServiceDataWrapperDTO getUsersOnGroup(GetUserOnGroupDTO userOnGroupDTO);
}
