/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.common.dto.group;

import com.isk.smartkey.persistence.entities.Group;

/**
 *
 * @author Hossam ElDeen
 */
public class GroupDTO {
    String secretKey;
    String name;
    String serialNumber;
    
    public GroupDTO() {
    }

    public GroupDTO(Group group){
        this.copyGroup(group);
    }

    public GroupDTO(String secretKey, String name, String groupId) {
        this.secretKey = secretKey;
        this.name = name;
        this.serialNumber = groupId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getSecretKey() {
        return secretKey;
    }
    
    public void copyGroup(Group group){
        setSecretKey(group.getSecretKey());
        setSerialNumber(group.getId());
        setName(group.getName());
    }
}
