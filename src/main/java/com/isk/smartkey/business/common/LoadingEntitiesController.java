/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.common;

import com.isk.smartkey.business.common.dto.businessResponse.ServiceStatusDTO;
import com.isk.smartkey.interceptor.logging.LoggingInterceptor;
import com.isk.smartkey.persistence.dao.SmartKeyDao;
import com.isk.smartkey.persistence.dao.group.GroupDao;
import com.isk.smartkey.persistence.entities.Group;
import com.isk.smartkey.persistence.entities.Permission;
import com.isk.smartkey.persistence.entities.User;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Hossam ElDeen
 */
@Repository
public class LoadingEntitiesController {

    private static final Logger logger = org.apache.log4j.Logger.getLogger(LoggingInterceptor.class);

    @Autowired(required = true)
    SmartKeyDao smartKeyDao;

    @Autowired(required = true)
    GroupDao groupDao;

    public Object loadUser(int userId) {
        User user;
        try {
            user = (User) smartKeyDao.load(User.class, userId);
        } catch (Exception ex) {
            return new ServiceStatusDTO("FAILED", "an internal error has been happened.");
        }

        if (user == null) {
            return new ServiceStatusDTO("FAILED", "user does not exist.");
        } else {
            return user;
        }
    }

    @Transactional
    public Object loadUserWithGroupsInitialized(int userId) {
        User user;
        try {
            user = (User) smartKeyDao.load(User.class, userId);
        } catch (Exception ex) {
            return new ServiceStatusDTO("FAILED", "an internal error has been happened.");
        }

        if (user == null) {
            return new ServiceStatusDTO("FAILED", "user does not exist.");
        } else {
            Hibernate.initialize(user.getUserGroupLinksList());
            return user;
        }
    }

    public Object loadPermission(int permissionId) {
        Permission permission;
        try {
            permission = (Permission) smartKeyDao.load(Permission.class, permissionId);
        } catch (Exception ex) {
            return new ServiceStatusDTO("FAILED", "an internal error has been happened.");
        }

        if (permission == null) {
            return new ServiceStatusDTO("FAILED", "permission does not exist.");
        } else {
            return permission;
        }
    }

    @Transactional
    public Object loadPermissionWithUserGroupInitialized(int permissionId) {
        Permission permission;
        try {
            permission = (Permission) smartKeyDao.load(Permission.class, permissionId);
        } catch (Exception ex) {
            return new ServiceStatusDTO("FAILED", "an internal error has been happened.");
        }

        if (permission == null) {
            return new ServiceStatusDTO("FAILED", "permission does not exist.");
        } else {
            Hibernate.initialize(permission.getUserGroupLinksList());
            return permission;
        }
    }

    public Object loadGroup(int groupId) {
        Group group;
        try {
            group = (Group) smartKeyDao.load(Group.class, groupId);
        } catch (Exception ex) {
            return new ServiceStatusDTO("FAILED", "an internal error has been happened.");
        }

        if (group == null) {
            return new ServiceStatusDTO("FAILED", "group does not exist.");
        } else {
            return group;
        }
    }

    public Object loadGroup(String groupId) {
        Group group;
        try {
            group = (Group) smartKeyDao.load(Group.class, groupId);
        } catch (Exception ex) {
            return new ServiceStatusDTO("FAILED", "an internal error has been happened.");
        }

        if (group == null) {
            return new ServiceStatusDTO("FAILED", "group does not exist.");
        } else {
            return group;
        }
    }

    @Transactional
    public Object loadGroupWithUserGroupsInitialized(int groupId) {
        Group group;
        try {
            group = (Group) smartKeyDao.load(Group.class, groupId);
        } catch (Exception ex) {
            logger.error("unable to load group", ex);
            return new ServiceStatusDTO("FAILED", "an internal error has been happened.");
        }

        if (group == null) {
            return new ServiceStatusDTO("FAILED", "group does not exist.");
        } else {
            Hibernate.initialize(group.getUserGroupLinksList());
            return group;
        }
    }

    @Transactional
    public Object loadGroupWithUserGroupsInitialized(String groupId) {
        Group group;
        try {
            group = (Group) smartKeyDao.load(Group.class, groupId);
        } catch (Exception ex) {
            logger.error("unable to load group", ex);
            return new ServiceStatusDTO("FAILED", "an internal error has been happened.");
        }

        if (group == null) {
            return new ServiceStatusDTO("FAILED", "group does not exist.");
        } else {
            Hibernate.initialize(group.getUserGroupLinksList());
            return group;
        }
    }

}
