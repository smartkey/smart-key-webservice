/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.common.dto.permission;

import com.isk.smartkey.persistence.entities.Permission;

/**
 *
 * @author Hossam ElDeen
 */
public class PermissionDTO {
    private boolean manageUsers;
    private boolean showLog;
    private boolean showPinMessages;
    private boolean showBattery;

    public PermissionDTO() {
    }
    
    public PermissionDTO(Permission permission){
        this.copyPermission(permission);
    }

    public PermissionDTO(boolean manageUsers, boolean logView, boolean pinMessage, boolean battery) {
        this.manageUsers = manageUsers;
        this.showLog = logView;
        this.showPinMessages = pinMessage;
        this.showBattery = battery;
    }

    public boolean isManageUsers() {
        return manageUsers;
    }

    public void setManageUsers(boolean manageUsers) {
        this.manageUsers = manageUsers;
    }

    public boolean isShowLog() {
        return showLog;
    }

    public void setShowLog(boolean showLog) {
        this.showLog = showLog;
    }

    public boolean isShowPinMessages() {
        return showPinMessages;
    }

    public void setShowPinMessages(boolean showPinMessages) {
        this.showPinMessages = showPinMessages;
    }

    public boolean isShowBattery() {
        return showBattery;
    }

    public void setShowBattery(boolean showBattery) {
        this.showBattery = showBattery;
    }
    
    public void copyPermission(Permission permission){
        setShowBattery(permission.isBattery());
        setShowLog(permission.isLogView());
        setManageUsers(permission.isManageUsers());
        setShowPinMessages(permission.isPinMessage());
    }
}
