/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.isk.smartkey.business.common;

import com.isk.smartkey.business.common.dto.businessResponse.ServiceStatusDTO;
import com.isk.smartkey.persistence.entities.Group;
import com.isk.smartkey.persistence.entities.User;
import com.isk.smartkey.persistence.entities.UserGroupLink;
import org.springframework.stereotype.Component;

/**
 *
 * @author Hossam ElDeen
 */
@Component
public class BusinessUtil {
    public static ServiceStatusDTO checkUserHasGroup(User user, Group group) {
        for (Object userGroupLink : user.getUserGroupLinksList()) {
            if (((UserGroupLink) userGroupLink).getGroup().getId().equals(group.getId())) {
                return null;
            }
        }
        return new ServiceStatusDTO("FAILED", "user does not belong to this group");
    }
}
