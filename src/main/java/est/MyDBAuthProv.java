/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package est;

import com.isk.smartkey.business.common.dto.businessResponse.ServiceDataWrapperDTO;
import com.isk.smartkey.business.login.LoginController;
import com.isk.smartkey.business.login.dto.user.UserCredentialsDto;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author Hossam ElDeen
 */
public class MyDBAuthProv implements AuthenticationProvider, Serializable {

    @Autowired
    LoginController loginController;

    @Override
    public Authentication authenticate(Authentication a) throws AuthenticationException {
        String userName = (String) a.getPrincipal();
        String password = (String) a.getCredentials();

        ServiceDataWrapperDTO login = loginController.login(new UserCredentialsDto(userName, password));
        List<GrantedAuthority> authorities = null;
        if (login.getData() != null) {
            authorities = new ArrayList<GrantedAuthority>();
            authorities.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return "ROLE_USER";
                }
            });
        }

        Authentication authentication = new UsernamePasswordAuthenticationToken(userName, password, authorities);
        return authentication;
    }

    @Override
    public boolean supports(Class<?> type) {
        return true;
    }

}
